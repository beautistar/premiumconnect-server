const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");
const multer = require("multer");
//var upload = multer({ dest: "./Images" });

const userController = require("../Controller/user");

module.exports = function(app) {
  // app.get("/", function(req, res) {
  //   res.render("index.ejs");
  // });

  router.use(function(req, res, next) {
    // console.log("request", req);
    const token = req.body.token || req.query.token || req.headers.token;

    if (token) {
      jwt.verify(token, keys.secretOrKey, function(err, decoded) {
        if (err) {
          return res.json({
            status: 401,
            success: false,
            message: "Failed to authenticate Token"
          });
        } else {
          //console.log("dd", decoded);
          req.user = decoded;
          next();
        }
      });
    } else {
      res.status(403).send({
        success: false,
        message: "no token provided"
      });
    }
  });

  app.use("/api", router);

  app.post("/register", userController.register); //Register Api
  app.put("/api/uploadimage", userController.uploadImage);
  app.post("/login", userController.login); //Login Api
  app.post("/loginWithEmail", userController.loginWithEmail); //login With Email
  app.post("/api/logout", userController.LogOut); //LogOut Api
  app.get("/updateVerify/:id", userController.updateVerify); // update verify true
  app.put("/api/changePassword", userController.changePassword); //change password
  app.get("/api/userprofile", userController.userProfile); // Get User Profile
  app.post("/api/users", userController.allUser); // All Users
  app.get("/api/phoneUsers", userController.allPhoneUser); //All PhoneNumber Users
  app.put("/api/updateprofile", userController.updateUserProfile); // update User Profile
  //
  app.post("/forgot", userController.forgot); //send mail to user fr reset password
  app.get("/reset/:id", userController.reset); //get link nd render forgot page
  app.post(
    "/updatePasswordViaEmail/:id",
    userController.updatePasswordViaEmail
  ); //forgotpassword
  app.put("/api/updatelocation", userController.updateLocation); //updateLocation
  app.put("/api/updatelastseen", userController.updateLastseen); //update lastseen
  app.put("/api/EmsPayment", userController.EmsPayment); //EMS Payment
  app.put("/api/GroupPayment", userController.GroupPayment); // Group Payment
  app.get("/api/EmsExpireDate", userController.EmsExpireDate); // Get EMS ExpireDate
  app.get("/api/EmsSubscription", userController.EmsSubscription); //Get Ems-Subscription T/F
  app.put("/api/ActiveEmsService", userController.ActiveEmsService); //update ActiveEmsService T/F
  app.get("/api/GetEmergencyContact", userController.GetEmergencyContact); //get emergency contact
  app.put("/api/AddEmergencyContact", userController.AddEmergencyContact); //Add emergency contact
  app.put("/api/RemoveEmergencyContact", userController.RemoveEmergencyContact); //Remove emergency contact

  //Groups Api's  -------------------------------------------------
  app.post("/api/creategroup", userController.createGroup); //Create Group
  app.post("/api/createDevGroup", userController.createAllCityGroup); // Create All City Group
  app.put("/api/updategroup", userController.updateGroup); //Update Group
  app.delete("/api/deletegroup", userController.deletegroup); //Delete Group
  app.delete("/api/leftGroup", userController.leftGroup); //left Group
  app.post("/api/getallgroup", userController.getallgroup); //Get All group of User
  app.post("/api/getgroup", userController.getGroup); //getgroup
  app.get("/api/mygroup", userController.myGroup); //Get isSuperAdmin Group
  app.post("/api/adduser", userController.addUser); //Add User in Group
  app.delete("/api/removeuser", userController.removeUser); //Remove User from Group
  app.put("/api/updateuser", userController.updateGroupMember); //Update User In Group
  app.get("/api/transactions", userController.transactions); //get trasaction
  app.post("/api/uploadaudio", userController.chatAudioAdd);
  app.put("/api/groupNotificationMessage", userController.groupEvent); //update Group Event
  app.get("/api/groupIds", userController.getGroupIds); //get groupId's of loginUser
  //
  app.post("/api/sendSms", userController.sendSMS); //send SMS to all groupMember who have a phoneNumber
  app.post("/twilioToken", userController.twilioToken);
  app.post("/voice", userController.voice);
  //Notification Api
  app.post("/api/getDeviceId", userController.getDeviceId);
  app.post("/api/pushNotification", userController.PushNotification);
  app.put("/api/updatestatus", userController.updateStatus); //update user status(Geen,Yellow,Red)

  //Message Reporting
  app.post("/api/reporting", userController.reporting);
  //blockUser
  app.put("/api/blockUser", userController.blockUser);

  //match deviseId
  app.post("/api/matchDeviceId", userController.matchDeviceId);

  //send otp
  app.post("/send-otp", userController.sendOtp);

  app.post("/in-app-purchase", userController.inAppPurchase);
};
