const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");
const _ = require("lodash");
var service = require("../service/service.js");
var waterfall = require("run-waterfall");
const mime = require("mime");
const moment = require("moment");
const request = require("request");
const Cryptr = require("cryptr");
const cryptr = new Cryptr(keys.secretOrKey);
//Load Models
const User = require("../Models/User");
const Groups = require("../Models/Groups");
const Payment = require("../Models/Payment");
const multer = require("multer");
//Load input Validation
//const validatorRegisterInput = require("../Validation/register");
//const validatorLoginInput = require("../Validation/login");
//const validatorGroupsInptut = require("../Validation/groups");

//For twilio
const VoiceResponse = require("twilio").twiml.VoiceResponse;
const ClientCapability = require("twilio").jwt.ClientCapability;
const AccessToken = require("twilio").jwt.AccessToken;
const callerId = "client:quick_start";
const callerNumber = keys.TwilioNumber;

//In-app purchase configuration
var iap = require("in-app-purchase");
iap.config({
  appleExcludeOldTransactions: true,
  applePassword: keys.applePassword,

  googleAccToken: keys.googleAccToken,
  googleClientID: keys.googleClientID,
  googleClientSecret: keys.googleClientSecret,

  googleServiceAccount: {
    clientEmail: keys.client_email,
    privateKey: keys.private_key
  }
  // verbose: true
});

//Set Storage Image
var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "./Images/");
  },
  filename: function(req, file, callback) {
    console.log("File--NAME", file.fieldname);
    console.log("filename-----------", file);
    callback(
      null,
      file.fieldname + "-" + Date.now() + file.originalname
      //+ "." + mime.extension(file.mimetype)
    );
  }
});
var upload = multer({ storage: storage }).array("avatar", 1);

///////////////////// USER Api's ///////////////////////////
//----------------------------------------------------------
//POST Register Api function
exports.register = function(req, res) {
  console.log("body--", req.body);

  waterfall(
    [
      function(callback) {
        var params = {
          fullName: req.body.fullName,
          countryCode: req.body.countryCode,
          phoneNumber: req.body.phoneNumber,
          emailId: req.body.emailId,
          password: req.body.password,
          deviceId: req.body.deviceId,
          signUp_with: req.body.signUp_with,
          socialId: req.body.socialId,
          image: req.body.image
        };
        service.registeUser(params, req).then((user, err) => {
          if (err) {
            callback(err, user);
          } else {
            callback(null, user);
          }
        });
      }
    ],
    function(err, user) {
      if (err) {
        // console.log("err----", err);
        res.json({
          status: 500,
          message: err
        });
      } else {
        // console.log("user---", user);
        res.json({
          status: 200,
          data: user
        });
      }
    }
  );
};

//----------------------------------------------------------
//PUT  Update verify key true
exports.updateVerify = function(req, res) {
  const decryptedId = cryptr.decrypt(req.params.id);
  User.findByIdAndUpdate(
    { _id: decryptedId },
    { $set: { verify: true } },
    { new: true }
  ).then(user => {
    if (user) {
      res.render("../views/confirmVerification.ejs", { emailId: user.emailId });
      // res.json({
      //   status: "Success",
      //   user: user
      // });
    } else {
      res.render("../views/notConfirmVerification.ejs");
      // res.json({
      //   status: "Fail",
      //   message: "user Not Found"
      // });
    }
  });
};

//----------------------------------------------------------
//Upload Image in User
exports.uploadImage = function(req, res) {
  let fullUrl = keys.serverUrl;
  //req.protocol + "://" + req.get("host");
  // console.log("Full--Url", fullUrl);

  upload(req, res, function(err, data) {
    if (err) {
      // console.log("ERR---", err);
      return res.json({ error_description: err });
    } else {
      // console.log("body", req.body);

      // console.log("REQ--", req.files);
      if (req.files.length != 0) {
        // console.log("A-Name----", req.files[0].path);
        var imagePath = fullUrl + "/" + req.files[0].path;

        var userId = mongoose.Types.ObjectId(req.body.id);
        if (err) {
          return res.json({ error_description: err });
          // callback(err);
        } else {
          // further code
          waterfall(
            [
              function(callback) {
                var params = {
                  userId: userId,
                  image: imagePath
                };
                service.updateImageInput(params, req).then((image, err) => {
                  if (err) {
                    callback(err, image);
                  } else {
                    callback(null, image);
                  }
                });
              }
            ],
            function(err, image) {
              // console.log("image", image);
              if (err) {
                res.json({
                  status: 500,
                  err: err
                });
              } else {
                res.json({
                  status: 200,
                  data: image
                });
              }
            }
          );
        }
      }
    }
  });
};

//----------------------------------------------------------
//POST Login With PhoneNumber Api
exports.login = function(req, res) {
  console.log("Body----", req.body);
  waterfall(
    [
      function(callback) {
        var params = {
          phoneNumber: req.body.phoneNumber
        };
        service.loginUser(params, req).then((user, err) => {
          if (err) {
            callback(err, user);
          } else {
            callback(null, user);
          }
        });
      }
    ],
    (err, user) => {
      if (err) {
        // console.log("err---", err);
        res.json({
          status: 500,
          err: err
        });
      } else {
        // console.log("user---", user);
        res.json({
          status: 200,
          data: user
        });
      }
    }
  );
};

//----------------------------------------------------------
//Post Login With E-mailId Api
exports.loginWithEmail = function(req, res) {
  console.log("Body----", req.body);
  waterfall(
    [
      function(callback) {
        var params = {
          emailId: req.body.emailId
        };
        service.loginWithEmailInput(params, req).then((user, err) => {
          if (err) {
            callback(err, user);
          } else {
            callback(null, user);
          }
        });
      }
    ],
    (err, user) => {
      if (err) {
        // console.log("err---", err);
        res.json({
          status: 500,
          err: err
        });
      } else {
        // console.log("user---", user);
        res.json({
          status: 200,
          data: user
        });
      }
    }
  );
};

//----------------------------------------------------------
//Get User Profile
exports.userProfile = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  User.find({ _id: loginUser }).then(user => {
    if (!user) {
      res.json({
        status: "Fail",
        message: "User Not found"
      });
    } else {
      res.json({
        status: "Success",
        data: user
      });
    }
  });
};

//----------------------------------------------------------
//GET All User detail
exports.allUser = function(req, res) {
  // console.log("Body---", req.body);
  //var loginUser = mongoose.Types.ObjectId(req.user.id);
  User.find()
    .select("-__v")
    .lean()
    .exec()
    .then(users => {
      const FinalUsers = _.orderBy(users, user => user.fullName.toLowerCase());
      // console.log("Users---", users);
      // console.log("FinalUsers---", FinalUsers);
      if (!users) {
        res.json({
          status: "Fail",
          message: "Users Not Found"
        });
      } else {
        res.json({
          status: "Success",
          data: FinalUsers
        });
      }
    });
};

//----------------------------------------------------------
//Get All User who have phone number
exports.allPhoneUser = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  //console.log("DD-id", loginUser);
  Groups.aggregate(
    [
      {
        $match: {
          "membersList.memberId": loginUser
        }
      },
      {
        $unwind: "$membersList"
      },
      { $group: { _id: "$membersList.memberId" } },
      {
        $lookup: {
          from: "users",
          as: "userData",
          localField: "_id",
          foreignField: "_id"
        }
      },
      {
        $match: { "userData.phoneNumber": { $ne: null } }
      }
    ],
    (err, users) => {
      // console.log("Users--", users);
      var AllUsers = [];
      users.forEach(user => {
        if (user.userData[0]) {
          AllUsers.push(user.userData[0]);
        }
      });
      //console.log("Users--", AllUsers);

      if (err) {
        res.json({
          status: "Fail",
          err: err
        });
      } else {
        res.json({
          status: "Success",
          data: AllUsers
        });
      }
    }
  );
};

//----------------------------------------------------------
//Update User Profile
exports.updateUserProfile = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  User.findByIdAndUpdate(
    { _id: loginUser },
    {
      $set: {
        fullName: req.body.fullName,
        emailId: req.body.emailId,
        countryCode: req.body.countryCode,
        phoneNumber: req.body.phoneNumber
      }
    },
    { new: true }
  ).then(user => {
    if (!user) {
      res.json({
        status: "Fail",
        message: "User Profile Not Update"
      });
    } else {
      res.json({
        status: "Success",
        message: "User Profile Update Successfully",
        data: user
      });
    }
  });
};

//----------------------------------------------------------
//POST forgot password
exports.forgot = function(req, res) {
  var emailId = req.body.emailId;

  if (req.body.emailId === "") {
    return res.json("Email is required");
  } else {
    waterfall(
      [
        function(callback) {
          var params = {
            emailId: emailId
          };
          service.forgotInput(params, req).then((user, err) => {
            if (err) {
              callback(err, user);
            } else {
              callback(null, user);
            }
          });
        }
      ],
      function(err, user) {
        if (err) {
          res.json({
            status: "Fail",
            message: err
          });
        } else {
          res.json({
            status: user.status,
            data: user
          });
        }
      }
    );
  }
};

//----------------------------------------------------------
// GET Reset password
exports.reset = function(req, res) {
  waterfall(
    [
      function(callback) {
        console.log("PP--", req.params);
        service.resetInput(req).then((user, err) => {
          if (err) {
            callback(err, user);
          } else {
            callback(null, user);
          }
        });
      }
    ],
    function(err, user) {
      if (err) {
        res.json({
          status: 500,
          message: err
        });
      } else {
        if (user.status == "Success") {
          res.render("../views/forgot.ejs", { _id: req.params.id });
        } else {
          res.render("../views/expiredForgot.ejs");
        }
        // res.json({
        //   status: 200,
        //   message: user
        // });
      }
    }
  );
};

//----------------------------------------------------------
//PUT updatePasswordViaEmail
exports.updatePasswordViaEmail = function(req, res) {
  waterfall(
    [
      function(callback) {
        service.forgotPasswordInput(req).then((message, err) => {
          if (err) {
            callback(err, message);
          } else {
            callback(null, message);
          }
        });
      }
    ],
    function(err, message) {
      if (err) {
        res.json({
          status: message.status,
          message: err.message
        });
      } else {
        res.json({
          status: message.status,
          data: message.message
        });
      }
    }
  );
};

//----------------------------------------------------------
//Update User Location WF-Pending
exports.updateLocation = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  const updateLocation = {
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    updateTime: new Date()
  };
  console.log("Updating-Lat-Long--", updateLocation);

  var fieldtoset = { location: updateLocation, status: req.body.status };
  User.findByIdAndUpdate({ _id: loginUser }, fieldtoset, {
    new: true
  }).then((updateUser, err) => {
    if (err) {
      res.json({
        status: "Fail",
        err: err
      });
    } else {
      if (updateUser) {
        console.log("User-Name:---", updateUser.fullName);
        console.log("Status--", updateUser.status);

        res.json({
          status: "Success",
          message: "location Update Successfully",
          data: updateUser
        });
      } else {
        res.json({
          status: "Fail",
          message: "Location Not Updated"
        });
      }
    }
  });
  // console.log("Body---", req.body);
  // var loginUser = mongoose.Types.ObjectId(req.user.id);
  // var latitude = req.body.latitude;
  // var longitude = req.body.longitude;
  // var lat_int = Number(latitude);
  // var long_int = Number(longitude);
  // // console.log("TypeOf--", typeof lat_int);
  // // console.log("TypeOf--", typeof req.body.latitude);

  // waterfall(
  //   [
  //     function(callback) {
  //       var params = {
  //         loginUser: loginUser,
  //         latitude: lat_int,
  //         longitude: long_int
  //       };
  //       service.updateLocationInput(params, req).then((user, err) => {
  //         if (err) {
  //           callback(err, user);
  //         } else {
  //           callback(null, user);
  //         }
  //       });
  //     }
  //   ],
  //   function(err, user) {
  //     if (err) {
  //       res.json({
  //         status: 500,
  //         user: err
  //       });
  //     } else {
  //       res.json({
  //         status: 200,
  //         data: user
  //       });
  //     }
  //   }
  // );
};

//----------------------------------------------------------
//Lastseen of User  WF-Pending
exports.updateLastseen = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  var flag = req.body.online;
  // const update = {
  //   online: true,
  //   status: "green"
  // };
  // update.location = {
  //   updateTime: new Date()
  // };

  if (flag == true) {
    User.findOneAndUpdate(
      { _id: loginUser },
      {
        $set: {
          online: true,
          status: "green",
          lastseen: new Date()
          // "location.updateTime": new Date()
        }
      },
      { new: true }
    )
      .then(update => {
        //For Admin User
        if (req.user.isAdmin) {
          console.log("isAdmin-", req.user.isAdmin);
          res.json(update);
        } else {
          //For Other User check subscription

          //console.log("update---", update);
          //check if User Ems Payment date is Expired then subscription 'false'
          if (update.subscription == true) {
            var currentDate = moment(new Date());
            var expireDate = update.EmsPayExpireDate;
            //console.log("expireDate", expireDate);
            if (currentDate > expireDate) {
              User.findOneAndUpdate(
                { _id: user.id },
                { $set: { subscription: false } },
                { new: true }
              )
                .then(subscriptionUpdate => {
                  res.json(subscriptionUpdate);
                })
                .catch(err => res.json(err));
            } else {
              // console.log("Update---", update);
              res.json(update);
            }
          } else {
            res.json(update);
          }
        }
      })
      .catch(err => res.json(err));
  } else {
    var setOffline = {
      online: false,
      status: "red",
      lastseen: new Date()
    };
    User.findByIdAndUpdate({ _id: loginUser }, setOffline, { new: true })
      .then(update => {
        // console.log("Update---", update);
        res.json(update);
      })
      .catch(err => res.json(err));
  }
};

/////////////////////Group Api's ///////////////////////////

//----------------------------------------------------------
//POST Create Group API
exports.createGroup = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  let fullUrl = keys.serverUrl;
  //req.protocol + "://" + req.get("host");
  upload(req, res, function(err, data) {
    if (err) {
      return res.json({ error_description: err });
    } else {
      if (req.files.length != 0) {
        var imagePath = fullUrl + "/" + req.files[0].path;
      } else {
        var imagePath =
          "http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/GroupImage/groups.png";
      }
      // const { errors, isValid } = validatorGroupsInptut(req.body);
      // if (!isValid) {
      //   return res.status(400).json(errors);
      // } else {
      waterfall(
        [
          function(callback) {
            var params = {
              loginUser: loginUser,
              groupName: req.body.groupName,
              groupType: req.body.groupType,
              groupImage: imagePath
            };
            service.createGroupInput(params, req).then((group, err) => {
              if (err) {
                callback(err, group);
              } else {
                callback(null, group);
              }
            });
          }
        ],
        function(err, group) {
          // console.log("Groups-- ", group);
          if (err) {
            res.json({
              status: 500,
              message: err
            });
          } else {
            res.json({
              status: 200,
              data: group
            });
          }
        }
      );
      // }
    }
  });

  //check validation
};

//----------------------------------------------------------
//PUT Update Group API
exports.updateGroup = function(req, res) {
  const groupId = mongoose.Types.ObjectId(req.body.groupId);
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  waterfall(
    [
      function(callback) {
        var params = {
          groupId: groupId,
          loginUser: loginUser
        };
        service.updateGroupInput(params, req).then((group, err) => {
          if (err) {
            callback(err, group);
          } else {
            callback(null, group);
          }
        });
      }
    ],
    function(err, group) {
      if (err) {
        res.json({
          status: 500,
          message: err
        });
      } else {
        res.json({
          status: 200,
          data: group
        });
      }
    }
  );
};

//----------------------------------------------------------
//GET All group of user
exports.getallgroup = function(req, res) {
  var groupType = req.body.groupType;
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  waterfall(
    [
      function(callback) {
        var params = {
          groupType: groupType,
          loginUser: loginUser
        };
        service.getAllGroupInput(params, req).then((groups, err) => {
          // console.log("groups---", groups);
          // console.log("Err--", err);
          if (err != undefined) {
            callback(err, groups);
          } else {
            callback(null, groups);
          }
        });
      }
    ],
    function(err, groups) {
      // console.log("groups---", groups);
      if (err) {
        res.json({
          status: 500,
          message: err
        });
      } else {
        res.json({
          status: 200,
          data: groups
        });
      }
    }
  );
};

//----------------------------------------------------------
//Get groups
exports.getGroup = function(req, res) {
  var groupId = mongoose.Types.ObjectId(req.body.groupId);
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  waterfall(
    [
      function(callback) {
        var params = {
          loginUser: loginUser,
          groupId: groupId
        };
        service.GetGroup(params, req).then((group, err) => {
          if (err) {
            callback(err, group);
          } else {
            callback(null, group);
          }
        });
      }
    ],
    function(err, group) {
      if (err) {
        res.json({
          status: 500,
          err: err
        });
      } else {
        res.json({
          status: 200,
          data: group
        });
      }
    }
  );
};

//----------------------------------------------------------
//Get All Group when User isSuperAdmin
exports.myGroup = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  var groupType = req.body.groupType;
  var query = [
    {
      $unwind: "$membersList"
    },
    {
      $match: {
        $and: [
          { "membersList.memberId": loginUser },
          { "membersList.isSuperAdmin": true }
        ]
      }
    }
  ];

  if (req.body.groupType == null) {
    Groups.aggregate(query, (err, group) => {
      if (err) res.json(err);
      //console.log("isSuperAdmin---Group", group);
      if (group.length != 0) {
        res.json({
          status: 200,
          message: "there isSuperAdmin user group",
          group: group
        });
      } else {
        res.json({
          status: 400,
          message: "You have to not create any Group"
        });
      }
    });
  } else {
    res.json({
      status: 400,
      message: "there is no group"
    });
  }
};
//----------------------------------------------------------
//DELETE delete group by isSuperAdmin
exports.deletegroup = function(req, res) {
  var groupId = mongoose.Types.ObjectId(req.body.groupId);
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  waterfall(
    [
      function(callback) {
        var params = {
          groupId: groupId,
          loginUser: loginUser
        };

        service.deletGroupInput(params, req).then((group, err) => {
          if (err) {
            callback(err, group);
          } else {
            callback(null, group);
          }
        });
      }
    ],
    function(err, group) {
      if (err) {
        res.json({
          status: 500,
          message: err
        });
      } else {
        res.json({
          status: 200,
          data: group
        });
      }
    }
  );
};

//----------------------------------------------------------
//Left Group Api
exports.leftGroup = function(req, res) {
  var groupId = mongoose.Types.ObjectId(req.body.groupId);
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  waterfall(
    [
      function(callback) {
        var params = {
          groupId: groupId,
          loginUser: loginUser
        };

        service.leftGroupInput(params, req).then((group, err) => {
          if (err) {
            callback(err, group);
          } else {
            callback(null, group);
          }
        });
      }
    ],
    function(err, group) {
      if (err) {
        res.json({
          status: 500,
          message: err
        });
      } else {
        res.json({
          status: 200,
          data: group
        });
      }
    }
  );
};

//----------------------------------------------------------
//POST Add User in group
exports.addUser = function(req, res) {
  var groupId = mongoose.Types.ObjectId(req.body.groupId);
  var memberId = mongoose.Types.ObjectId(req.body.memberId);
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  waterfall(
    [
      function(callback) {
        var params = {
          groupId: groupId,
          memberId: memberId,
          loginUser: loginUser
        };
        service.addUserInGroup(params, req).then((user, err) => {
          if (err) callback(err);
          console.log("UU", user);
          callback(null, user);
        });
      }
    ],
    function(err, user) {
      if (err) {
        res.json({
          status: 500,
          message: err
        });
      } else {
        res.json({
          status: 200,
          data: user
        });
      }
    }
  );
};

//----------------------------------------------------------
// Delete User From Group
exports.removeUser = function(req, res) {
  var groupId = mongoose.Types.ObjectId(req.body.groupId);
  var memberId = mongoose.Types.ObjectId(req.body.memberId);
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  waterfall(
    [
      function(callback) {
        var params = {
          groupId: groupId,
          memberId: memberId,
          loginUser: loginUser
        };
        service.removeUserInput(params, req).then((user, err) => {
          if (err) callback(err, user);
          //console.log("UU", user);
          callback(null, user);
        });
      }
    ],
    function(err, user) {
      if (err) {
        res.json({
          status: 500,
          message: err
        });
      } else {
        res.json({
          status: 200,
          data: user
        });
      }
    }
  );
};

//----------------------------------------------------------
//PUT Update User by superAdmin In Group
exports.updateGroupMember = function(req, res) {
  //

  var groupId = mongoose.Types.ObjectId(req.body.groupId);
  var memberId = mongoose.Types.ObjectId(req.body.memberId);
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  waterfall(
    [
      function(callback) {
        var params = {
          groupId: groupId,
          memberId: memberId,
          loginUser: loginUser
        };
        service.updateGroupMemberInput(params, req).then((user, err) => {
          if (err) callback(err, user);
          callback(null, user);
        });
      }
    ],
    function(err, user) {
      if (err) {
        res.json({
          status: 500,
          message: err
        });
      } else {
        res.json({
          status: 200,
          data: user
        });
      }
    }
  );
};

//----------------------------------------------------------
//Get User Transactions
exports.transactions = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);

  waterfall(
    [
      function(callback) {
        var params = {
          loginUser: loginUser
        };
        service.getTransactions(params, req).then((data, err) => {
          if (err) {
            callback(err, data);
          } else {
            callback(null, data);
          }
        });
      }
    ],
    function(err, data) {
      if (err) {
        res.json({
          status: 500,
          err: err
        });
      } else {
        res.json({
          status: 200,
          data: data
        });
      }
    }
  );
};

//----------------------------------------------------------
//chat Audio & Images Api
exports.chatAudioAdd = function(req, res) {
  //console.log("REQ_BODY--", req.body);
  let fullUrl = keys.serverUrl;
  //req.protocol + "://" + req.get("host");
  //console.log("Full--Url", fullUrl);
  upload(req, res, function(err, data) {
    if (err) {
      return res.json({ error_description: err });
    } else {
      if (req.files.length != 0) {
        var AudioName = fullUrl + "/" + req.files[0].path;
        //console.log("Audio-Name---", AudioName);
        res.json({
          status: "Success",
          data: AudioName
        });
      }
    }
  });
};

//////////////////////////////////////////////////////////
//-----------------------------------------------------------IMP
//create all city group by developer
exports.createAllCityGroup = async function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  var data = req.user.cityLocation;
  var query = [
    {
      $match: {
        creater: "Developer",
        cityName: data.cityName,
        city_latitude: data.city_latitude,
        city_longitude: data.city_longitude
      }
    }
  ];

  await Groups.aggregate(query, (err, isMatch) => {
    if (err) {
      res.json({
        status: "Fail",
        err: err
      });
    } else {
      // console.log("isMatch--", isMatch);

      if (isMatch.length != 0) {
        Groups.aggregate(
          [
            {
              $match: { _id: isMatch[0]._id }
            },
            {
              $unwind: "$membersList"
            },
            {
              $match: { "membersList.memberId": loginUser }
            }
          ],
          (err, userMatch) => {
            if (err) {
              res.json({
                status: "Fail",
                err: err
              });
            } else {
              //console.log("User---Match", userMatch);
              if (userMatch.length != 0) {
                res.json({
                  status: "Fail",
                  message: `User Already In ${data.city}`
                });
              } else {
                var storedata = {
                  memberId: loginUser,
                  isSuperAdmin: false,
                  isAdmin: false
                };
                Groups.updateOne(
                  { _id: isMatch[0]._id },
                  { $push: { membersList: storedata } },
                  { new: true },
                  (err, group) => {
                    //console.log("Add-User---", group);
                    if (err) {
                      res.json({
                        status: "Fail",
                        err: err
                      });
                    } else {
                      res.json({
                        status: "Success",
                        data: group
                      });
                    }
                  }
                );
              }
            }
          }
        );
      } else {
        var imagePath =
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPItSrMWmQ6kzCbAqIA_Lt_xC8L4I2YnPb5MVo-CoU7ICp5jP8CA";

        const GroupField = {
          groupName: `${data.cityName} Group`,
          groupType: "PUBLIC",
          creater: "Developer",
          createrId: null,
          createdByAdmin: false,
          groupImage: imagePath,
          subscription: true,
          cityName: data.cityName,
          city_latitude: data.city_latitude,
          city_longitude: data.city_longitude
        };

        GroupField.event = {
          title: null,
          eventDate: null,
          eventTime: null,
          message: null,
          eventLocation: null
        };

        GroupField.membersList = [];
        let member = {
          memberId: loginUser,
          isSuperAdmin: false,
          isAdmin: false
        };
        GroupField.membersList.push(member);

        new Groups(GroupField).save().then((group, err) => {
          //console.log("Create-group---", group);
          if (err) {
            res.json({
              status: "Fail",
              err: err
            });
          } else {
            res.json({
              status: "Success",
              data: group
            });
          }
        });
      }
    }
  });
};

//-----------------------------------------------------------
//LogOut User Api
exports.LogOut = function(req, res) {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  console.log("TOken-User---", loginUser);
  //mongoose.Types.ObjectId(req.user.id);
  waterfall(
    [
      function(callback) {
        var params = {
          loginUser: loginUser
        };
        service.logout(params, req).then((logout, err) => {
          if (err) {
            callback(err, logout);
          } else {
            callback(null, logout);
          }
        });
      }
    ],
    (err, logout) => {
      if (err) {
        res.json({
          status: 500,
          err: err
        });
      } else {
        res.json({
          status: 200,
          data: logout
        });
      }
    }
  );
};

//-----------------------------------------------------------
//group Event Push notification
exports.groupEvent = function(req, res) {
  console.log("RB--", req.body);
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  var groupId = mongoose.Types.ObjectId(req.body.groupId);
  var query = [
    {
      $match: { _id: groupId }
    },
    {
      $unwind: "$membersList"
    },
    {
      $match: {
        $and: [
          { "membersList.memberId": loginUser },
          { "membersList.isSuperAdmin": true }
        ]
      }
    }
  ];
  var query1 = [
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.body.groupId),
        "membersList.memberId": mongoose.Types.ObjectId(req.user.id)
      }
    },
    {
      $lookup: {
        from: "users",
        as: "userData",
        localField: "membersList.memberId",
        foreignField: "_id"
      }
    },
    {
      $project: {
        "userData.deviceId": 1,
        groupName: 1
      }
    }
  ];

  Groups.aggregate(query, (err, group) => {
    //console.log("Group---", group);
    // resolve(group);
    var eventData = {
      title: req.body.title,
      eventDate: req.body.eventDate,
      eventTime: req.body.eventTime,
      message: req.body.message,
      eventLocation: req.body.eventLocation
    };
    Groups.findByIdAndUpdate(
      { _id: groupId },
      { $set: { event: eventData } },
      { new: true }
    ).then(updateEvent => {
      var eventData = updateEvent.event;
      console.log("Update-Event", updateEvent);
      // send action notification to all group-members
      Groups.aggregate(query1, (err, data) => {
        if (err) {
          res.json({
            status: "Fail",
            err: err
          });
        } else {
          if (data != 0) {
            //console.log("Data---", data);
            var groupId = data[0]._id;
            var title = data[0].groupName;
            var groupType = data[0].groupType;
            var arrDeviceId = data[0].userData;
            var AllIds = [];
            arrDeviceId.forEach(device => {
              if (device.deviceId) {
                if (device.deviceId == req.user.deviceId) {
                } else {
                  AllIds.push(device.deviceId);
                }
              }
            });

            //console.log("AllIds---", AllIds);

            //function for send notification
            function sendMessageToUser(deviceId) {
              request(
                {
                  url: "https://fcm.googleapis.com/fcm/send",
                  method: "POST",
                  headers: {
                    "Content-Type": " application/json",
                    Authorization: keys.notificationServerKey
                  },
                  body: JSON.stringify({
                    registration_ids: AllIds,
                    content_available: true,
                    // message: 'first messsage'
                    notification: {
                      title: title,
                      body: req.body.message,
                      sound: "default",
                      priority: "high",
                      show_in_foreground: true,
                      timeToLive: 60 * 60 * 24
                    },
                    data: {
                      groupId: groupId,
                      groupName: title,
                      groupType: groupType,
                      type: "event",
                      event: eventData
                    }
                  })
                },
                (error, respoonse, body) => {
                  //console.log("output... ", body, "\n", respoonse.statusCode);
                  if (error) {
                    //console.log("error... ", error);
                  } else {
                    //console.log("no error ");
                    res.json({
                      notificationStatus: true,
                      status: "Success",
                      data: updateEvent
                    });
                  }
                }
              );
            }
            //call sendMessageToUser function
            sendMessageToUser(req.user.deviceId);
          } else {
            res.json({
              status: "Fail",
              message: "data not found"
            });
          }
        }
      });
    });
  });
};
//-----------------------------------------------------------
//push notification
exports.getDeviceId = (req, res) => {
  console.log("RB--GetDeviceData-----------", req.body);
  var query = [
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.body.groupId),
        "membersList.memberId": mongoose.Types.ObjectId(req.user.id)
      }
    },
    {
      $unwind: "$membersList"
    },
    {
      $lookup: {
        from: "users",
        as: "userData",
        localField: "membersList.memberId",
        foreignField: "_id"
      }
    },
    {
      $match: {
        "userData.online": { $eq: false }
      }
    },
    {
      $project: {
        "userData.deviceId": 1,
        "userData.notificationBadge": 1,
        "userData._id": 1,
        groupName: 1,
        groupType: 1
      }
    }
  ];
  Groups.aggregate(query, (err, data) => {
    // console.log("Data---", data);
    // console.log("Id---", req.user.id);
    if (err) {
      res.json({
        status: "Fail",
        err: err
      });
    } else {
      if (data != 0) {
        //console.log("Data---", data);
        var groupId = data[0]._id;
        var title = data[0].groupName;
        var groupType = data[0].groupType;
        var AllIds = [];
        console.log("---------------GetData-Start------------");
        data.forEach(Data => {
          if (Data.userData[0].deviceId) {
            if (Data.userData[0].deviceId == req.user.id) {
              //
            } else {
              AllIds.push(Data.userData[0]);
            }
          } else {
            //
          }
          //console.log("UserData---", Data.userData[0]);
        });
        res.json({
          status: "Success",
          deviceId: AllIds,
          groupId: groupId,
          title: title,
          groupType: groupType
        });
        console.log("---------------GetData-End------------");
      } else {
        res.json({
          status: "Fail",
          message: "data not found"
        });
      }
    }
  });
};

exports.PushNotification = (req, res) => {
  console.log("RB---", req.body);
  var DeviceIds = req.body.deviceids;
  //console.log("DeviceIds", DeviceIds);
  DeviceIds.forEach(id => {
    sendMessageToUser(id.deviceId, id.notificationBadge, id._id);
  });
  console.log("-------------FCM-Start---------------");
  //function for send notification
  function sendMessageToUser(deviceId, notiBadge, UID) {
    //console.log("FCM-Function-Start-");
    request(
      {
        url: "https://fcm.googleapis.com/fcm/send",
        method: "POST",
        headers: {
          "Content-Type": " application/json",
          Authorization: keys.notificationServerKey
        },
        body: JSON.stringify({
          //registration_ids: DeviceIds,
          to: deviceId,
          content_available: true,
          time_to_live: 10,
          // message: 'first messsage'
          notification: {
            title: req.body.title,
            // body: req.body.message,
            body: req.body.message,
            sound: "default",
            priority: "high",
            show_in_foreground: true,
            badge: notiBadge + 1
          },
          // options: {
          //   priority: "high",
          //   timeToLive: 60 * 60 * 24
          // },
          data: {
            groupId: req.body.groupId,
            groupName: req.body.title,
            groupType: req.body.groupType
          }
        })
      },
      (error, response, body) => {
        console.log("output... ", body, "\n", response.statusCode);
        if (error) {
          console.log("error... ", error);
        } else {
          console.log("no error ");
          var Uid = mongoose.Types.ObjectId(UID);
          User.findByIdAndUpdate(
            { _id: Uid },
            { $set: { notificationBadge: notiBadge + 1 } },
            { new: true }
          ).catch();
          // //res.json({ status: true });
        }
      }
    );
    //console.log("FCM-Function-End-");
  }
  console.log("-------------FCM-End--------------");
};

//-----------------------------------------------------------
//Update Status of User (green,yellow,red)
exports.updateStatus = (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  User.findByIdAndUpdate(
    { _id: loginUser },
    { $set: { status: req.body.status } },
    { new: true }
  ).then((user, err) => {
    //console.log("User-Data---", user);
    if (err) throw err;
    if (user) {
      var cstatus = user.status;
      console.log("Update-Status--", {
        Name: user.fullName,
        status: user.status
      });
      res.json({
        status: "Success",
        data: cstatus
      });
    } else {
      res.json({
        status: "Fail",
        message: "User Not Found or Not Updated"
      });
    }
  });
};

/////////////////////Ems Api's ///////////////////////////
//------------------------------------------------------------
//Payment For Ems
exports.EmsPayment = (req, res) => {
  console.log("Req-body", req.body);
  var currentDate = moment(new Date());
  var expireDate;
  var recipet = req.body.receipt;
  //console.log("receipt", recipet);
  var plan = recipet.productId.split(".");
  var subscriptionType = plan[plan.length - 2];
  var month = plan[plan.length - 1];
  // console.log("plan----", plan);
  // console.log("month----", month);
  // console.log("subscriptionType----", subscriptionType);
  //check month ans set expireDate month wise
  if (month == "1month") {
    expireDate = moment(currentDate)
      .add(1, "M")
      .subtract(1, "day");
    //console.log("expireDate---", expireDate);
  } else if (month == "3month") {
    expireDate = moment(currentDate)
      .add(3, "M")
      .subtract(1, "day");
    //console.log("expireDate---", expireDate);
  } else if (month == "6month") {
    expireDate = moment(currentDate)
      .add(6, "M")
      .subtract(1, "day");
    // console.log("expireDate---", expireDate);
  } else {
    expireDate = moment(currentDate)
      .add(12, "M")
      .subtract(1, "day");
    //console.log("expireDate---", expireDate);
  }

  //In feature Add emergency contact when user subscribe EMS

  // var EmergencyContacts = [];
  // req.body.emergencyContacts.forEach(contact => {
  //   //console.log("Contact", contact);
  //   EmergencyContacts.push({
  //     name: contact.name,
  //     countryCode: contact.countryCode,
  //     phoneNumber: contact.phoneNumber
  //   });
  // });
  User.findByIdAndUpdate(
    { _id: req.user.id },
    {
      $set: {
        receipt: req.body.receipt,
        subscription: true,
        subscriptionType: subscriptionType,
        activeEmsService: true,
        EmsPayDate: currentDate,
        EmsPayExpireDate: expireDate,
        platform: req.body.platform
        // emergencyContacts: EmergencyContacts
      }
    },
    { new: true }
  ).then((user, err) => {
    //console.log("User---", user);
    var CurrentPayment = user.receipt;
    if (err) throw err;
    else {
      if (user) {
        const PaymentFields = {
          payFor: "EMS",
          receipt: req.body.receipt,
          subscriptionType: subscriptionType,
          userId: req.user.id,
          Amount: req.body.Amount,
          payDate: currentDate,
          payExpireDate: expireDate
        };

        new Payment(PaymentFields).save().then(payment => {
          res.json({
            status: "Success",
            message: "Payment Successfully",
            data: payment,
            CurrentPayment,
            user
          });
        });
      } else {
        res.json({
          status: "Fail",
          message: "User Not Found"
        });
      }
    }
  });
};

//------------------------------------------------------------
//For Private Group Payment
exports.GroupPayment = (req, res) => {
  var currentDate = moment(new Date());
  var futureMonth = moment(currentDate)
    .add(1, "M")
    .subtract(1, "day");

  const GroupField = {
    subscription: true,
    payDate: currentDate,
    payExpireDate: futureMonth
  };
  Groups.findByIdAndUpdate(
    { _id: req.body.groupId },
    { $set: { GroupField } },
    { new: true }
  ).then((group, err) => {
    if (err) throw err;
    else {
      if (group) {
        const PaymentFields = {
          payFor: "Group",
          receipt: req.body.receipt,
          userId: group.createrId,
          groupId: group._id,
          groupName: group.groupName,
          Amount: req.body.Amount,
          payDate: currentDate,
          payExpireDate: futureMonth
        };
        new Payment(PaymentFields)
          .save()
          .then(payment => {
            resolve({
              status: "Success",
              message: "Payment successfully Created",
              data: group,
              payment
            });
          })
          .catch(err => reject(err));
      }
    }
  });
};

//------------------------------------------------------------
// 7-Days Before get message Api
exports.EmsExpireDate = (req, res) => {
  User.findOne({ _id: req.user.id }).then(user => {
    if (user) {
      var data = {
        expireDate: user.EmsPayExpireDate,
        subscription: user.subscription,
        activeEmsService: user.activeEmsService
      };
      var expireDate = user.EmsPayExpireDate;
      if (user.subscription == true) {
        res.json({
          status: "Success",
          data: data
        });
      } else {
        res.json({
          status: "Fail",
          message: "User EmsPayment is Expired or Not Pay"
        });
      }
    } else {
      res.json({
        status: "Fail",
        message: "User Not Found"
      });
    }
  });
};

//------------------------------------------------------------
//Get User EMS Subscription true/false
exports.EmsSubscription = async (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  // User.findOne({ _id: loginUser }).then(user => {
  // const subdata = {
  //   subscription: user.subscription,
  //   subscriptionType: user.subscriptionType,
  //   activeEmsService: user.activeEmsService
  // };
  //   //console.log("message", subdata);
  //   if (!user) {
  //     res.json({
  //       status: "Fail",
  //       message: "User Not found"
  //     });
  //   } else {
  //     res.json({
  //       status: "Success",
  //       data: subdata
  //     });
  //   }
  // });

  //-------------------------------------------------------------
  var user = await User.findOne({ _id: loginUser });
  if (!user) {
    res.json({
      status: "Fail",
      message: "User not found"
    });
  } else {
    if (user.subscription) {
      //for apple
      if (user.platform == "apple") {
        var data = user.receipt.transactionReceipt;

        iap.setup(err => {
          console.log("In-SetUp---");
          if (err) {
            console.log(err);
          } else {
            console.log("Above- Validation---");
            iap.validate(iap.APPLE, data, async (err, response) => {
              console.log("In-Validation---");
              if (err) {
                console.log(err);
              } else {
                if (iap.isValidated(response)) {
                  console.log("Response--", response);
                  var purcahseDataList = iap.getPurchaseData(response);
                  console.log("Purchase---", purcahseDataList);

                  var expDt = purcahseDataList[0].expirationDate;
                  var ExpireDate = new Date(moment.unix(expDt / 1000));
                  var currentDate = new Date();
                  console.log("currentDate---", currentDate);

                  console.log("Rs-Date---", expDt);
                  console.log(
                    "ExpireDD--",
                    new Date(moment.unix(expDt / 1000))
                  );
                  if (moment().isAfter(ExpireDate)) {
                    //is Expired
                    //update subscription and payExpireDate
                    var updateFields = {
                      subscription: false,
                      activeEmsService: false,
                      EmsPayExpireDate: ExpireDate
                    };

                    var result = await User.findOneAndUpdate(
                      { _id: loginUser },
                      { $set: updateFields },
                      { new: true }
                    );

                    if (!result) {
                      res.json({
                        status: "Fail",
                        message: "User subscription data not updated"
                      });
                    } else {
                      var ssdata = {
                        subscription: result.subscription,
                        subscriptionType: result.subscriptionType,
                        activeEmsService: result.activeEmsService
                      };
                      res.json({
                        status: "Success",
                        message: "Data Successfully updated",
                        data: ssdata
                      });
                    }
                  } else {
                    var result = await User.findOneAndUpdate(
                      { _id: loginUser },
                      { $set: { EmsPayExpireDate: ExpireDate } },
                      { new: true }
                    );
                    var ssudata = {
                      subscription: user.subscription,
                      subscriptionType: user.subscriptionType,
                      activeEmsService: user.activeEmsService
                    };
                    res.json({
                      status: "Success",
                      message: "user subcription data",
                      data: ssudata
                    });
                  }
                } else {
                  res.json({
                    status: "Error",
                    message: "something wrong when validate receipt"
                  });
                }
              }
            });
          }
        });
      } else if (user.platform == "android") {
        // console.log("loginUser--", user);
        var GoogleReciept = {
          packageName: "com.premiumconnect",
          productId: user.receipt.productId,
          purchaseToken: user.receipt.purchaseToken,
          subscription: true // if the receipt is a subscription, then true
        };
        iap
          .validate(GoogleReciept)
          .then(async response => {
            if (iap.isValidated(response)) {
              // console.log("Lists---", response);
              var currentDate = new Date();
              var purchaseData = iap.getPurchaseData(response);
              console.log("purchaseData----", purchaseData);
              // valid receipt
              var googleExpDate = purchaseData[0].expirationDate;
              var ExpireDD = new Date(moment.unix(googleExpDate / 1000));
              console.log("ExpireDate---", ExpireDD);

              if (moment().isAfter(ExpireDD)) {
                console.log("In -Expire------------");
                var updateDatt = {
                  subscription: false,
                  activeEmsService: false,
                  EmsPayExpireDate: ExpireDD
                };
                var result = await User.findOneAndUpdate(
                  { _id: loginUser },
                  { $set: updateDatt },
                  { new: true }
                );
                if (!result) {
                  res.json({
                    status: "Fail",
                    message: "purchase data not updated in db"
                  });
                } else {
                  var Assdata = {
                    subscription: result.subscription,
                    subscriptionType: result.subscriptionType,
                    activeEmsService: result.activeEmsService
                  };

                  res.json({
                    status: "Success",
                    message: "purchase data updated successfully",
                    data: Assdata
                  });
                }
              } else {
                console.log("Not-Expire----");
                var update = await User.findOneAndUpdate(
                  { _id: loginUser },
                  { $set: { EmsPayExpireDate: ExpireDD } },
                  { new: true }
                );

                if (!update) {
                  res.json({
                    status: "Fail",
                    message: "purchase data not updated in db"
                  });
                } else {
                  var AssDD = {
                    subscription: update.subscription,
                    subscriptionType: update.subscriptionType,
                    activeEmsService: update.activeEmsService
                  };

                  res.json({
                    status: "Success",
                    message: "purchase data updated successfully",
                    data: AssDD
                  });
                }
              }

              // //----------------------
              // if (!purchaseData[0].autoRenewing) {
              //   console.log("------- Subscription CANCEL ----");
              //   console.log("cancelDate---", purchaseData[0].cancellationDate);
              //   var cancelDate = new Date(
              //     moment.unix(purchaseData[0].cancellationDate / 1000)
              //   );
              //   console.log("Cancel-Date---", cancelDate);

              //   var updatedd = {
              //     subscription: false,
              //     activeEmsService: false,
              //     EmsPayExpireDate: cancelDate
              //   };

              //   var result = await User.findOneAndUpdate(
              //     { _id: loginUser },
              //     { $set: updatedd },
              //     { new: true }
              //   );

              //   if (!result) {
              //     res.json({
              //       status: "Fail",
              //       message: "purchase data not updated in db"
              //     });
              //   } else {
              //     var Assdata = {
              //       subscription: result.subscription,
              //       subscriptionType: result.subscriptionType,
              //       activeEmsService: result.activeEmsService
              //     };

              //     res.json({
              //       status: "Success",
              //       message: "purchase data updated successfully",
              //       data: Assdata
              //     });
              //   }
              // } else {
              //   console.log("------- Subscription ON ----");
              //   var updatePurchaseData = {
              //     EmsPayExpireDate: ExpireDD
              //   };

              //   var dataUpdated = await User.findOneAndUpdate(
              //     { _id: loginUser },
              //     { $set: updatePurchaseData },
              //     { new: true }
              //   );

              //   if (!dataUpdated) {
              //     res.json({
              //       status: "Fail",
              //       message: "purchase data not updated in db"
              //     });
              //   } else {
              //     var Assudata = {
              //       subscription: dataUpdated.subscription,
              //       subscriptionType: dataUpdated.subscriptionType,
              //       activeEmsService: dataUpdated.activeEmsService
              //     };

              //     res.json({
              //       status: "Success",
              //       message: "purchase data updated successfully",
              //       data: Assudata
              //     });
              //   }
              // }
            } else {
              //
              // console.log("Cancel---");
              res.json({
                status: "Fail",
                message: "Purchase token not verifyed"
              });
            }
          })
          .catch(error => {
            // error...
            res.json({
              status: "Fail",
              message: "Something Wrong while verifying receipt"
            });
          });
      } else {
        var NssData = {
          subscription: user.subscription,
          subscriptionType: user.subscriptionType,
          activeEmsService: user.activeEmsService
        };
        res.json({
          status: "Fail",
          message: "User Not have any platform",
          data: NssData
        });
      }
    } else {
      //usen not subscribe
      const subdata = {
        subscription: user.subscription,
        subscriptionType: user.subscriptionType,
        activeEmsService: user.activeEmsService
      };
      res.json({
        status: "Fail",
        message: "user not subscribe EMS Service.",
        data: subdata
      });
    }
  }
};

//------------------------------------------------------------
// user can ActiveEmsService on/off means true/false
exports.ActiveEmsService = (req, res) => {
  User.findByIdAndUpdate(
    { _id: req.user.id },
    { $set: { activeEmsService: req.body.activeEmsService } },
    { new: true }
  ).then((user, err) => {
    //console.log("User---", user);
    if (user) {
      res.json({
        status: "Success",
        subscription: user.subscription,
        activeEmsService: user.activeEmsService
      });
    } else {
      res.json({
        status: "Fail"
      });
    }
  });
};

//-------------------------------------------------------------
// change Password
exports.changePassword = (req, res) => {
  User.findOne({ _id: req.user.id }).then(user => {
    if (user) {
      bcrypt.compare(req.body.password, user.password).then(isMatch => {
        if (isMatch) {
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(req.body.newPassword, salt, (err, hash) => {
              if (err) throw err;
              var newPassword = hash;
              User.findByIdAndUpdate(
                { _id: req.user.id },
                { $set: { password: newPassword } },
                { new: true }
              ).then(updatePassword => {
                if (updatePassword) {
                  res.json({
                    status: "Success",
                    message: "newPassword updated",
                    data: updatePassword
                  });
                } else {
                  res.json({
                    status: "Fail",
                    message: "newPassword Not Updated"
                  });
                }
              });
            });
          });
        } else {
          res.json({
            status: "Fail",
            message: "Password incorrect"
          });
        }
      });
    } else {
      res.json({
        status: "Fail",
        message: "User Not Found"
      });
    }
  });
};

//-------------------------------------------------------------
//getting groupId's of loginUser
exports.getGroupIds = (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  Groups.aggregate(
    [
      {
        $match: { "membersList.memberId": loginUser }
      },
      {
        $project: { _id: 1 }
      }
    ],
    (err, groupIds) => {
      console.log("groupIds--", groupIds);
      var AllIds = [];
      if (err) {
        res.json(err);
      } else {
        if (groupIds[0] != null) {
          groupIds.forEach(id => {
            AllIds.push(id._id);
          });
          res.json({
            status: "Success",
            data: AllIds
          });
        } else {
          res.json({
            status: "Fail",
            message: "GroupId's Not Found"
          });
        }
      }
    }
  );
};

//Send SMS Api
//-----------------------------------------------------------
exports.sendSMS = (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  const accountSid = keys.TwilioAccountSid;
  const authToken = keys.TwilioAuthToken;
  const client = require("twilio")(accountSid, authToken);

  User.findOne({ _id: loginUser }).then(user => {
    if (user.emergencyContacts.length != 0) {
      counter = 0;
      user.emergencyContacts.forEach(data => {
        counter++;
        var userNumber = "+" + data.countryCode + "" + data.phoneNumber;

        client.messages
          .create({
            body:
              "Hi I am ' " +
              req.user.fullName +
              " '\n" +
              "PLEASE SEND HELP, I’M IN AN ACCIDENT" +
              "\n" +
              "\n" +
              "https://www.google.com/maps/dir//" +
              req.body.lat +
              "," +
              req.body.long,
            from: keys.TwilioNumber,
            to: userNumber
          })
          .then(message => console.log(message.sid))
          .done();
      });

      if (user.emergencyContacts.length == counter) {
        res.json({
          status: "Success"
        });
      } else {
        //
      }
    } else {
      res.json({
        status: "Fail",
        message: "No Emergency Contact Found"
      });
    }
  });
};
//-------------------------------------------------------------
//Get User Emergency Contact Number
exports.GetEmergencyContact = (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  User.findOne({ _id: loginUser }).then(user => {
    console.log("User-emergencyContacts--", user.emergencyContacts);
    if (user.emergencyContacts.length != 0) {
      res.json({
        status: "Success",
        data: user.emergencyContacts
      });
    } else {
      res.json({
        status: "Fail",
        message: "Emergency ContactList is Empty"
      });
    }
  });
};
//-------------------------------------------------------------
// Add Emergency Contact
exports.AddEmergencyContact = (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  var EmergencyContacts = [];
  req.body.emergencyContacts.forEach(contact => {
    //console.log("Contact", contact);
    EmergencyContacts.push({
      name: contact.name,
      countryCode: contact.countryCode,
      phoneNumber: contact.phoneNumber
    });
  });
  User.findByIdAndUpdate(
    { _id: loginUser },
    { $push: { emergencyContacts: EmergencyContacts } },
    { new: true }
  ).then(user => {
    console.log("User--", user);
    res.json({
      status: "Success",
      data: user.emergencyContacts
    });
  });
};

//-------------------------------------------------------------
//Remove Emergency Contact
exports.RemoveEmergencyContact = (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  User.findOne({ _id: loginUser }).then(user => {
    console.log("User---", user);
    console.log("EmergencyContact----", user.emergencyContacts);

    _.remove(
      user.emergencyContacts,
      user => user._id == req.body.removeContactId
    );

    console.log("Remove-User---", user.emergencyContacts);

    User.findByIdAndUpdate(
      { _id: loginUser },
      { emergencyContacts: user.emergencyContacts },
      { new: true }
    ).then(updateUser => {
      console.log("Update-User---", updateUser);
      res.json({
        status: "Success",
        data: updateUser
      });
    });
  });
};

//-----------------------------------------------------------
// //Twilio Token
exports.twilioToken = (req, res) => {
  // //const AccessToken = require("twilio").jwt.AccessToken;
  console.log("AccessToken", AccessToken);
  const VoiceGrant = AccessToken.VoiceGrant;

  // Used when generating any kind of tokens
  const twilioAccountSid = keys.TwilioAccountSid;
  const twilioApiKey = keys.TwilioApiKey;
  const twilioApiSecret = keys.TwilioApiSecret;

  // Used specifically for creating Voice tokens
  const outgoingApplicationSid = keys.TwilioApplicationSid;
  const identity = "user";

  // Create a "grant" which enables a client to use Voice as a given user
  const voiceGrant = new VoiceGrant({
    outgoingApplicationSid: outgoingApplicationSid
  });

  // Create an access token which we will sign and return to the client,
  // containing the grant we just created
  const token = new AccessToken(
    twilioAccountSid,
    twilioApiKey,
    twilioApiSecret
  );
  token.addGrant(voiceGrant);
  token.identity = identity;

  // Serialize the token to a JWT string
  console.log(token.toJwt());
  res.json({ token: token.toJwt() });
};

//-------------------------------------------------------
//Voice
exports.voice = (req, res) => {
  console.log("RB---", req.body);
  // app.post('/voice', (req, res) => {
  // Create TwiML response
  const twiml = new VoiceResponse();
  if (req.body.To) {
    const dial = twiml.dial({ callerId: callerNumber });
    if (/^[\d\+\-\(\) ]+$/.test(req.body.To)) {
      dial.number(req.body.To);
    } else {
      dial.client(req.body.To);
    }
    //console.log("ABC--", abc.attributes.toString());
  } else {
    twiml.say("Thanks for calling!");
  }

  res.set("Content-Type", "text/xml");
  res.send(twiml.toString());
};

//--------------------------------------------------------
//User Reporting
exports.reporting = (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  //
  User.findOne({ _id: loginUser }).then(user => {
    // console.log("User--", user);
    if (!user) {
      res.json({
        status: "Fail",
        message: "User Not Found"
      });
    } else {
      res.json({
        status: "Success",
        message: "User Reported Successfully"
      });
    }
  });
};

//--------------------------------------------------------
//user Block in Group by superAdmin
exports.blockUser = (req, res) => {
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  var groupId = mongoose.Types.ObjectId(req.body.groupId);
  var memberId = mongoose.Types.ObjectId(req.body.memberId);

  var query = [
    {
      $match: { _id: groupId }
    },
    {
      $project: {
        membersList: {
          $filter: {
            input: "$membersList",
            as: "item",
            cond: {
              $and: [
                { $eq: ["$$item.memberId", loginUser] },
                {
                  // $or: [
                  // {
                  $eq: ["$$item.isSuperAdmin", true]
                  // },
                  // { $eq: ["$$item.isAdmin", true] }
                  // ]
                }
              ]
            }
          }
        }
      }
    }
  ];

  Groups.aggregate(query, (err, isMatch) => {
    if (err) reject(err);
    if (isMatch[0].membersList != 0) {
      Groups.findOneAndUpdate(
        {
          _id: groupId,
          "membersList.memberId": memberId
        },
        {
          $set: {
            "membersList.$.blockUser": req.body.blockUser
          }
        },
        {
          new: true
        }
      ).then(group => {
        //console.log("==group======", group);
        if (!group) {
          res.json({
            status: "Fail",
            message: "Member not block"
          });
        } else {
          res.json({
            status: "Success",
            message: "Member block Successfully",
            group: group
          });
        }
      });
    } else {
      res.json({
        status: "Fail",
        message: "You are not a superAdmin"
      });
    }
  });

  // Groups.findOneAndUpdate(
  //   { _id: groupId, "membersList.memberId": memberId },
  //   { $set: { "membersList.$blockUser": true } },
  //   { new: true }
  // ).then(result => {
  //   console.log("Result---", result);
  // });
};

//--------------------------------------------------------
//Match device ID and remove from Developer Group
exports.matchDeviceId = (req, res) => {
  console.log("RB---", req.body);
  var loginUser = mongoose.Types.ObjectId(req.user.id);
  var deviceID = req.body.deviceId;

  User.findOne({ _id: loginUser, deviceId: deviceID }).then(user => {
    console.log("------User--------", user);
    if (!user) {
      res.json({
        status: "Error",
        message: "deviceId not in db",
        logout: true
      });
    } else {
      res.json({
        status: "Success",
        message: "deviceId already in db",
        logout: false
      });
    }
  });
};

//--------------------------------------------------------
//Send OTP on phone number
exports.sendOtp = async (req, res) => {
  let params = req.body;
  const accountSid = keys.TwilioAccountSid;
  const authToken = keys.TwilioAuthToken;
  const client = require("twilio")(accountSid, authToken);

  try {
    if (!params.phoneNumber) {
      res.json({
        status: "Fail",
        message: "please enter require field"
      });
    } else {
      var randomDigit = await service.randomStringGenerator();

      console.log("Random-Digit---", randomDigit);

      if (randomDigit) {
        var userNumber = "+" + params.phoneNumber;
        //params.phoneNumber;
        client.messages
          .create({
            body:
              randomDigit + "  is your verification code for Premium Connect.",
            from: keys.TwilioNumber,
            to: userNumber
          })
          .then(message => {
            console.log(message.sid);
            if (!message) {
              res.json({
                status: "Fail",
                message: "Message not send from twillio"
              });
            } else {
              res.json({
                status: "Success",
                message: "OTP send successfully",
                otp: randomDigit
              });
            }
          })
          .catch(err => {
            res.json({
              status: "Fail",
              message: "number is wrong or something wrong while send OTP"
            });
          });
      } else {
        res.json({
          status: "Fail",
          message: "Random digit not genrerated"
        });
      }
    }
  } catch (e) {
    res.json({
      status: "Fail",
      message: "Something wrong"
    });
  }
};

//-------------------------------------------------------
//in-app purchase verifications

exports.inAppPurchase = async (req, res) => {
  // var iap = require("iap");
  // var base64 = require("base-64");

  var androidReceipt = {
    autoRenewingAndroid: true,
    signatureAndroid:
      "ZDauB9mwQrS3MwH2f9DGsoFJPr8+AUx53Lhf3Tn/8l09JRGVYqcYMPOP5Wt5blCI2DJVLlnTwpAW1pSK54kpH5ZHqAyZun3Ea5ogi2nI5gGF0qkxkFruudVR9O0wzoMGLWF23bJIVbivdcetMZ37j2M0ii+kao7efYXvTcEQvAEax5X8Wxej+m3qPIldc0hqAzqp92Av+EB6jgav3phSnVDsQcKAxSSWSJaxpL56zhFvjYcynFqFGdI5lhxg86kmiH6DtYucQRXktyjhaxHH0oX2nsoOJJzQWOU083ut0dfYWbU1f31AeEBv/gopA1T7YIm9tWzRD+mHcIKrEUR8dQ==",
    purchaseToken:
      "hemkfkinjmefdpfofmhniomf.AO-J1OwM6nIANwxJttkVO6sXzXimTQw6_bxbrsBHG0yWSsbepnR6TtUY6icBxSN7nBE4rPPocEOKgEP-GO3Kk-X0HcqlzKJWH1QF0y2hm1kVTr7Uv3nA6B3y4DGulF9y05ou3kCf01Na",
    dataAndroid:
      '{"orderId":"GPA.3312-9098-2329-22488","packageName":"com.premiumconnect","productId":"plan.premium.1month","purchaseTime":1564383568885,"purchaseState":0,"purchaseToken":"hemkfkinjmefdpfofmhniomf.AO-J1OwM6nIANwxJttkVO6sXzXimTQw6_bxbrsBHG0yWSsbepnR6TtUY6icBxSN7nBE4rPPocEOKgEP-GO3Kk-X0HcqlzKJWH1QF0y2hm1kVTr7Uv3nA6B3y4DGulF9y05ou3kCf01Na","autoRenewing":true}',
    transactionReceipt:
      '{"orderId":"GPA.3312-9098-2329-22488","packageName":"com.premiumconnect","productId":"plan.premium.1month","purchaseTime":1564383568885,"purchaseState":0,"purchaseToken":"hemkfkinjmefdpfofmhniomf.AO-J1OwM6nIANwxJttkVO6sXzXimTQw6_bxbrsBHG0yWSsbepnR6TtUY6icBxSN7nBE4rPPocEOKgEP-GO3Kk-X0HcqlzKJWH1QF0y2hm1kVTr7Uv3nA6B3y4DGulF9y05ou3kCf01Na","autoRenewing":true}',
    transactionDate: "1564383568885",
    transactionId: "GPA.3312-9098-2329-22488",
    productId: "plan.premium.1month"
  };

  var keyObject = {
    type: "service_account",
    project_id: "premiumconnect-53e40",
    private_key_id: "76d9cc2d304aebc9f1159b49a9182dd1445779b5",
    private_key:
      "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC7TniXhb6IwuKb\nX2xmK9PXgX3kdspgMQxuGogBmP6u4JU0mPjd3lx+z7oO1S8JoE81cCV/sjnLXTLQ\nv53l0bai0T+240vjCYhXp1NwQ6iDRsDK06Unw5KlY/bMcWntosYso/OtH+ioSL8s\nj9Y/67fQIk169miwS9m7L2l+lcngostDwq+j7SkYlKSbYNUkv9z1O19AYQb+aHo/\nTM2cTadYZfAZ+cB8sVp7MLzYVDkFMS6q7NMexF+em4A561X1C/WxcQxA7cTDTlBN\nOUSzt56Cfu5GkTvWzllvHpRGUEiyaJkG5rATiyvbWGHMBPNjxEvnkvv0Ca/x+rPQ\nLqs7nBFbAgMBAAECggEAAXt8K4F8zIrzotR9KyHVqghK/W/jPMz76DMq4w2u66YO\nCD3dr65+1OdtSt4Xt9/I61yuhKAzIjo5Q2WrfjzL++XkDIfSpe1WuDrex4NW5hMS\nm4hT75319HvMtuQpjnPnVZsb5pbbZIQ5icS++//a6I7Hm4gvs+rQbPwnEuq55PPh\nYA7CNQMfHzKR1RNlM+kICP/M3LTvTPG8wAnm841L4tXgR0D8dmSABpWyL6TLdC/n\nxCkn/5p8BP4Ekee4vyDgruX7b4NMcJq47hpjHS1GRDL/nFZhKqRHNv1BQC4eUQSN\nL82XezBTw9K5xTkCnqI/MDpmLR9/4vZG9Enu1VnP4QKBgQD7nFMVaqwnS5YFJh8n\nWQIFW5AYeutkGER2Clm9/3iAyTrTj1bUB7jpTbQ6QZIilg4s+QCFcDxdYnvB7f4h\nIQkiusf3tgkbV4CtA/wxY2tucRkv/NgNn4a4eXYRuYklhmhskyL8XVPoEGX9NvwN\nokwyaUf2jtzMj+HTN6KrwRosTQKBgQC+kvgG7EJBzsrIb2LGfPu69TnSsiVYQ7yE\nUUE3CfRbzNFjt2vIkXOhAy0obSSW78g2T2lDb3HRyz3nIvsIpSR645OG+MMfRLsc\noHHH2P19p3o8+HP+/Kdz6ZSmCXg/5Xwfxfiz/BGcj84Rr/Xj9Qf7UMJEJnythTl+\nlsrzVAToRwKBgCk3wqSPhQQq7biKNjBL9HjMNx7LhCQxlYWAzJLDdwoh7PEbWi4B\n1djiUdP5SedAjbHwWJWDfWAO8hci1a63qrd1waxitbs06m9tZ7LvhnnhLS8fTIVu\nzLMLsrvUmRL8ob6nm+ObplMt3IdOqpvRKopXlOJfJOyd5XCkfJGGYLNBAoGAEVkz\ntv2e8nquvxxUs5kBGX3la6bH+4b/DFSRST9gvVKh5R/3ceS7z/aID9wPrqXZZghI\nntjwVZbY5cerbpgKAB3jei//I0OQfQSmFvlL6jpqyE4d0SKS49xpqcvG5JoxeHr/\nLvNNbyEwMy//hGbz+Q7bAHU4eFteZoW3p/sID1UCgYBgcPrX/0fqJRhE7AHBSs97\n2B00MB2O8R9YHISD4KrA+yIuXlA0KnKJIOARILvViegOBkhyAu0PDezsgMnLwRxZ\ndrjDWKio4BFN0JR+iUWsecT0bnc/kg36vflN2GuItKyhGb3xjqWzC67YTYBK+5Aw\nRaoLB2tDD7apRb3JcLbYaQ==\n-----END PRIVATE KEY-----\n",
    client_email:
      "pcserviceaccount@premiumconnect-53e40.iam.gserviceaccount.com",
    client_id: "102791394941331035925",
    auth_uri: "https://accounts.google.com/o/oauth2/auth",
    token_uri: "https://oauth2.googleapis.com/token",
    auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
    client_x509_cert_url:
      "https://www.googleapis.com/robot/v1/metadata/x509/pcserviceaccount%40premiumconnect-53e40.iam.gserviceaccount.com"
  };

  // var platform = "google";
  // var payment = {
  //   receipt: receipt.toString(),
  //   productId: "plan.premium.1month",
  //   packageName: "com.premiumconnect",
  //   secret: "76d9cc2d304aebc9f1159b49a9182dd1445779b5",
  //   subscription: true, // optional, if google play subscription
  //   keyObject: keyObject, // required, if google
  //   excludeOldTransactions: true
  // };

  // iap.verifyPayment(platform, payment, function(error, response) {
  //   if (error) {
  //     console.log("Error--", error);
  //   } else {
  //     console.log("Response---", response);
  //   }
  //   /* your code */
  // });

  var iosReceipt = {
    transactionId: "1000000551188509",
    transactionDate: 1564139229000.0,
    transactionReceipt:
      "MIIT5AYJKoZIhvcNAQcCoIIT1TCCE9ECAQExCzAJBgUrDgMCGgUAMIIDhQYJKoZIhvcNAQcBoIIDdgSCA3IxggNuMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgEDAgEBBAMMATMwCwIBCwIBAQQDAgEAMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDAIBDgIBAQQEAgIAgTANAgENAgEBBAUCAwHWUDANAgETAgEBBAUMAzEuMDAOAgEJAgEBBAYCBFAyNTMwGAIBBAIBAgQQKbLGdMCEl+ZklIum7GBHVDAbAgEAAgEBBBMMEVByb2R1Y3Rpb25TYW5kYm94MBwCAQUCAQEEFHLiy2NwfDuusCMBTx3vjyCz9hYrMB0CAQICAQEEFQwTY29tLnByZW1pdW0uY29ubmVjdDAeAgEMAgEBBBYWFDIwMTktMDctMjZUMTE6MDc6MTBaMB4CARICAQEEFhYUMjAxMy0wOC0wMVQwNzowMDowMFowPwIBBgIBAQQ3Nkkn+1wjxg9qkx1FpMGPEwqvZ4qpZYZJUOwvcYvM0HZ7tU7FkD6KiJMUmuZPpN1NaHK+FHAf6zA/AgEHAgEBBDfhDkyccJQVoU/pIH++kdC91dRbT+O/mMBf3pr9Pev7Qa5U37psqX2uJC4aXiXIlo2Lu1Cyms9mMIIBhAIBEQIBAQSCAXoxggF2MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p4KqgjAbAgIGpwIBAQQSDBAxMDAwMDAwNTUxMTg4NTA5MBsCAgapAgEBBBIMEDEwMDAwMDA1NTExODg1MDkwHwICBqgCAQEEFhYUMjAxOS0wNy0yNlQxMTowNzowOVowHwICBqoCAQEEFhYUMjAxOS0wNy0yNlQxMTowNzoxMFowHwICBqwCAQEEFhYUMjAxOS0wNy0yNlQxMToxMjowOVowIgICBqYCAQEEGQwXc3ViLnBsYW4ucHJlbWl1bS4xbW9udGiggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAGKMqxbeLq7AFsjhnNnORljfwoz91AmzMy67aEgEpp85idY13znVhJbxEftRDFTuhY+WO3/Vsc+hfKF8D3kxZiyGh7gQc50dy1pPsNRf+J8Yn0j9ERmFzVCGJbDPqF53//qcPRuF5PYxRLMcRRP0jbGX0tQGk7bR/duHqrf0ESOnv1MGArXKlCdO8wwUz6YQ0TvTDpVIQnQkOWE5OqH1//QLuMOUZ73t1StsLzRL8ysckHiYfb+lR4twezFN03QXUclGthmrBKHQlYZiB2yqdLWsRo6P4LVByqyKf8eyyzhCAzVYKfQCcABjSEuzl9LNl/8HZu/mwlSQCKAWIXZULho=",
    productId: "sub.plan.premium.1month"
  };

  var googlePublicKey =
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmDMFLk/msWjFOAM6EpRlHHpD3qCwX/XOGZ2QlUhEsoc1x/D1SFF6yPaEYzPRPq5/aC2pZFqqlpKOShADZC51sZaBh3SPxEZS7i/lln2vqHhoVXnDV4wF+hi0Zun4OYUbzeyFEcBq5A4UcxXJ7GKhZ2HZ/58A5W7mguu8fxtiDm+QGT24c19o9U1CaemX7/ytL2Kr+FcFFBhwPDtAemWpoomM3wj8rWEUfvHpVBWvKMZCgNzpaXn25aSS44Wr75DrdJaIm1DhMUevk3CFEsz/NlSHA99ev7nw2PQ5sTG5ryD6PJh3JEtgptNcrrOR41EAjQ6sTNCmQgfPrO1evAJDMQIDAQAB";

  // var iap = require("in-app-purchase");
  // var validationType = iap.APPLE;
  // iap.config({ applePassword: "be0040a4df734dafa66044acb1892286" });
  // var data = iosReceipt.transactionReceipt;

  // iap.setup(err => {
  //   console.log("In-SetUp---");
  //   if (err) {
  //     console.log(err);
  //   } else {
  //     console.log("Above- Validation---");
  //     iap.validate(iap.APPLE, data, (err, response) => {
  //       console.log("In-Validation---");
  //       if (err) {
  //         console.log(err);
  //       } else {
  //         if (iap.isValidated(response)) {
  //           console.log("Response--", response);
  //           var purcahseDataList = iap.getPurchaseData(response);
  //           console.log("Purchase---", purcahseDataList);
  //         }
  //       }
  //     });
  //   }
  // });

  ///--------------Google-In-App-Purchase---------------

  var RR = {
    packageName: "com.premiumconnect",
    productId: androidReceipt.productId,
    purchaseToken: androidReceipt.purchaseToken,
    subscription: true // if the receipt is a subscription, then true
  };

  var iap = require("in-app-purchase");
  iap.config({
    appleExcludeOldTransactions: true,
    applePassword: "applePassword",

    googleAccToken:
      "ya29.GltUB_lpFvHd1D2DcS4S5d3LvEcMYlsUVOeO_ZMQ6BLzzjcEMG-WrBHdfYRjY9tQSSivghJUiq0nZzr682qHRUUaF4NzDS9Fxz2TvnQiCQsDHJ391x7VB-nJEf9C",
    googleRefToken: "1/B1wGTi64Pk7Qn-0v5hzkT83t7Zvx86iWAN-ChvfD1WU",
    googleClientID:
      "664483444581-iu97emrbh1inc00uq8gth4gr03kekjks.apps.googleusercontent.com",
    googleClientSecret: "QRWJTqTypqRNE-aHY6qF-0D0",

    googleServiceAccount: {
      clientEmail: keyObject.client_email,
      privateKey: keyObject.private_key
    },
    verbose: true
  });

  iap
    .validate(RR)
    .then(response => {
      if (iap.isValidated(response)) {
        // console.log("Lists---", response);
        var purchaseData = iap.getPurchaseData(response);
        console.log("purchaseData----", purchaseData);
        // valid receipt
        if (iap.isExpired(purchaseData[0])) {
          console.log("Purchase[0]---", purchaseData[0]);
          // receipt has been expired
        }
      } else {
        //
        console.log("Cancel---");
      }
    })
    .catch(error => {
      // error...
    });
};

//---------------------------------------------------
//twilio generate otp

exports.twilioGenerateOTP = async (req, res) => {
  const accountSid = keys.TwilioAccountSid;
  const authToken = keys.TwilioAuthToken;
  const PhoneVerification = require("twilio")(accountSid, authToken);
  // PhoneVerification.prototype.requestPhoneVerification = function(
  //   phone_number,
  //   country_code,
  //   via,
  //   callback
  // ) {
  request(
    "post",
    "/protected/json/phones/verification/start",
    {
      api_key: "",
      phone_number: phone_number,
      via: via,
      country_code: country_code,
      code_length: 4
    },
    function(response) {
      console.log("response", response);
    }
  );
  // };
};
