const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Payment Schema
const PaymentSchema = new Schema({
  payFor: {
    type: String
  },
  receipt: {
    type: Object
  },
  subscriptionType: {
    type: String
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: "user"
  },
  groupId: {
    type: Schema.Types.ObjectId,
    ref: "groups"
  },
  groupName: {
    type: String
  },
  Amount: {
    type: String,
    required: false
  },
  payDate: {
    type: Date,
    default: Date.now
  },
  payExpireDate: {
    type: Date
  }
});

module.exports = Payment = mongoose.model("payment", PaymentSchema);
