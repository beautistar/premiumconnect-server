const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create User Schema
const UserSchema = new Schema({
  // firstName: {
  //   type: String
  // },
  // lastName: {
  //   type: String
  // },
  platform: {
    type: String // android or apple
  },
  fullName: {
    type: String,
    required: false
  },
  emailId: {
    type: String,
    required: false
  },
  password: {
    type: String,
    required: false
  },
  countryCode: {
    type: Number,
    required: false
  },
  phoneNumber: {
    type: Number,
    required: false
  },
  deviceId: {
    type: String
  },
  isAdmin: {
    type: Boolean
  },
  signUp_with: {
    type: String
  },
  socialId: {
    type: String
  },
  image: {
    type: String
  },
  status: {
    type: String
  },
  subscription: {
    type: Boolean,
    default: false
  },
  subscriptionType: {
    type: String
  },
  activeEmsService: {
    type: Boolean,
    default: false
  },
  EmsPayDate: {
    type: Date
  },
  EmsPayExpireDate: {
    type: Date
  },
  location: {
    latitude: {
      type: Number
    },
    longitude: {
      type: Number
    },
    updateTime: {
      type: Date
    }
  },
  cityLocation: {
    cityName: {
      type: String
    },
    city_latitude: {
      type: String
    },
    city_longitude: {
      type: String
    }
  },
  emergencyContacts: [
    {
      name: {
        type: String
      },
      countryCode: {
        type: Number,
        required: false
      },
      phoneNumber: {
        type: Number,
        required: false
      }
    }
  ],
  receipt: {
    type: Object
  },
  verify: {
    type: Boolean,
    required: false
  },
  createDate: {
    type: Date,
    default: Date.now
  },
  updateDate: {
    type: Date,
    default: Date.now
  },
  online: {
    type: Boolean
  },
  lastseen: {
    type: Date
  },
  confirmPasswordTime: {
    type: Date,
    default: Date.now
  },
  notificationBadge: {
    type: Number
  }
});

module.exports = User = mongoose.model("users", UserSchema);
