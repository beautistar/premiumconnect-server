const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Groups Schema
const GroupsSchema = new Schema({
  groupName: {
    type: String,
    required: true
  },
  groupType: {
    type: String,
    required: true
  },
  creater: {
    type: String,
    required: false
  },
  createrId: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  createdByAdmin: {
    type: Boolean
  },
  groupImage: {
    type: String
  },
  createDate: {
    type: Date,
    default: Date.now
  },
  subscription: {
    type: Boolean,
    default: false
  },
  payDate: {
    type: Date
  },
  payExpireDate: {
    type: Date
  },
  cityName: {
    type: String
  },
  city_latitude: {
    type: String
  },
  city_longitude: {
    type: String
  },
  event: {
    title: {
      type: String
    },
    eventDate: {
      type: Date
    },
    eventTime: {
      type: Date
    },
    message: {
      type: String
    },
    eventLocation: {
      type: Object
    }
  },
  membersList: [
    {
      memberId: {
        type: Schema.Types.ObjectId,
        ref: "users"
      },
      createDate: {
        type: Date,
        default: Date.now
      },
      isSuperAdmin: {
        type: Boolean,
        required: false
      },
      isAdmin: {
        type: Boolean,
        required: false
      },
      lastSeen: {
        type: Date,
        default: Date.now
      },
      blockUser: {
        type: Boolean,
        default: false
      }
    }
  ],
  payment: {
    payType: {
      type: String,
      required: false
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: "users"
    },
    payFor: {
      type: String
    },
    Amount: {
      type: Number,
      required: false
    },
    Date: {
      type: Date
    },
    expireDate: {
      type: Date
    }
  }
});

module.exports = Groups = mongoose.model("groups", GroupsSchema);
