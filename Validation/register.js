const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validatorRegisterInput(data) {
  let errors = {};
  data.userName = !isEmpty(data.userName) ? data.userName : "";
  data.emailId = !isEmpty(data.emailId) ? data.emailId : "";
  data.phoneNumber = !isEmpty(data.phoneNumber) ? data.phoneNumber : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.password2 = !isEmpty(data.password2) ? data.password2 : "";
  //data.verify = !isEmpty(data.verify) ? data.verify : "";

  if (!Validator.isLength(data.userName, { min: 2, max: 30 })) {
    errors.userName = "Name must be between 2 to 30 character";
  }

  if (Validator.isEmpty(data.userName)) {
    errors.userName = "userName is required";
  }

  if (Validator.isEmpty(data.emailId)) {
    errors.emailId = "Email Field is Required";
  }

  if (!Validator.isEmail(data.emailId)) {
    errors.emailId = "Email is Invalid";
  }

  if (Validator.isEmpty(data.phoneNumber)) {
    errors.phoneNumber = "phoneNumber is required";
  }

  if (!Validator.isLength(data.phoneNumber, { max: 10 })) {
    errors.phoneNumber = "phoneNumber maximum 10 digit";
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Password is required";
  }

  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "password must be at least 6 characters";
  }

  if (Validator.isEmpty(data.password2)) {
    errors.password2 = "Confirm Password field is required";
  }

  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = "Password does not match";
  }

  //   if (Validator.isEmpty(data.verify)) {
  //     errors.verify = "roles is required";
  //   }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
