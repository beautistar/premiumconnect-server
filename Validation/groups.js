const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validatorGroupsInput(data) {
  let errors = {};
  data.groupName = !isEmpty(data.groupName) ? data.groupName : "";
  data.groupType = !isEmpty(data.groupType) ? data.groupType : "";
  data.payType = !isEmpty(data.payType) ? data.payType : "";
  //data.Amount = !isEmpty(data.Amount) ? data.Amount : "";
  //data.isSuperAdmin = !isEmpty(data.isSuperAdmin) ? data.isSuperAdmin : "";
  // data.isAdmin = !isEmpty(data.isAdmin) ? data.isAdmin : "";

  if (!Validator.isLength(data.groupName, { min: 2, max: 30 })) {
    errors.groupName = "Name must be between 2 to 30 character";
  }

  if (Validator.isEmpty(data.groupName)) {
    errors.groupName = "groupName is required";
  }

  if (Validator.isEmpty(data.groupType)) {
    errors.groupType = "groupType Field is Required";
  }

  if (!Validator.matches(data.groupType)) {
    errors.groupType = "groupType is Invalid";
  }

  // if (data.groupType == "PRIVATE") {
  //   if (Validator.isEmpty(data.payType)) {
  //     errors.payType = "PayType is Required";
  //   }
  //   if (Validator.isEmpty(data.Amount)) {
  //     errors.Amount = "Amount is Required";
  //   }
  // }

  // if (Validator.isEmpty(data.isSuperAdmin)) {
  //   errors.isSuperAdmin = "isSuperAdmin is required";
  // }

  // if (Validator.isEmpty(data.isAdmin)) {
  //   errors.isAdmin = "isAdmin is required";
  // }

  //   if (!Validator.isLength(data.isAdmin, { min: 6, max: 30 })) {
  //     errors.isAdmin = "isAdmin must be at least 6 characters";
  //   }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
