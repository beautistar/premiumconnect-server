const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateLoginInput(data) {
  let errors = {};

  data.emailId = !isEmpty(data.emailId) ? data.emailId : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  if (!Validator.isEmail(data.emailId)) {
    errors.emailId = "Email is invalid";
  }

  if (Validator.isEmpty(data.emailId)) {
    errors.emailId = "Email field is required";
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Password is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
