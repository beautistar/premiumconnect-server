const mongoose = require("mongoose");
const nodemailer = require("nodemailer");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const keys = require("../config/keys");
const moment = require("moment");
const request = require("request");
const Cryptr = require("cryptr");
const cryptr = new Cryptr(keys.secretOrKey);

//Load Models
const Groups = require("../Models/Groups");
const User = require("../Models/User");
const Payment = require("../Models/Payment");

const Service = require("./service");

/////////////////////User-Services//////////////////////////

//----------------------------------------------------------
//Register User Service
exports.registeUser = (params, req) => {
  var Data = req.body;
  // create User Fields
  const newUser = {
    fullName: Data.fullName,
    deviceId: Data.deviceId,
    isAdmin: false,
    online: true,
    status: "green",
    lastseen: new Date(),
    countryCode: Data.countryCode,
    signUp_with: Data.signUp_with,
    image: Data.image,
    subscription: false,
    subscriptionType: null,
    activeEmsService: false,
    notificationBadge: 0,
    cityLocation: {
      cityName: Data.cityName,
      city_latitude: Data.city_latitude,
      city_longitude: Data.city_longitude
    }
  };
  //update fields when user login with facebook/google
  const updateUser = {
    deviceId: params.deviceId,
    online: true,
    status: "green"
  };
  if (Data.latitude && Data.longitude) {
    newUser.location = {
      latitude: Data.latitude,
      longitude: Data.longitude,
      updateTime: new Date()
    };
  } else {
    newUser.location = {
      latitude: 42.8946441,
      longitude: 129.5606682,
      updateTime: new Date()
    };
  }
  return new Promise((resolve, reject) => {
    if (params.signUp_with == "facebook" || params.signUp_with == "google") {
      // console.log("-signUp_with-", params.signUp_with);
      User.aggregate(
        [
          {
            $match: { socialId: Data.socialId }
          }
        ],
        (err, user) => {
          // console.log("USER---", user);
          if (err) {
            reject(err);
          } else {
            if (user == 0) {
              //create new FB/Google User
              newUser.socialId = Data.socialId;
              newUser.emailId = null;
              newUser.countryCode = null;
              newUser.phoneNumber = null;
              newUser.verify = true;
              new User(newUser).save((err, register_user) => {
                //payload
                const payload = {
                  id: register_user.id,
                  fullName: register_user.fullName,
                  emailId: register_user.emailId,
                  deviceId: register_user.deviceId,
                  isAdmin: register_user.isAdmin,
                  phoneNumber: register_user.phoneNumber,
                  signUp_with: register_user.signUp_with,
                  cityLocation: register_user.cityLocation
                };
                if (err) {
                  reject(err);
                } else {
                  //Generate Token
                  Service.generateToken(
                    payload,
                    keys.secretOrKey,
                    register_user
                  ).then((token, err) => {
                    if (err) reject(err);
                    resolve(token);
                  });
                }
              });
            } else {
              //update user details
              updateUser.cityLocation = {
                cityName: Data.cityName,
                city_latitude: Data.city_latitude,
                city_longitude: Data.city_longitude
              };
              updateUser.lastseen = new Date();
              if (Data.latitude && Data.longitude) {
                updateUser.location = {
                  latitude: Data.latitude,
                  longitude: Data.longitude,
                  updateTime: new Date()
                };
              } else {
                updateUser.location = {
                  latitude: 42.8946441,
                  longitude: 129.5606682,
                  updateTime: new Date()
                };
              }
              //Login With Fb/Google & update `updateUser` fields
              User.findOneAndUpdate(
                { socialId: params.socialId },
                { $set: updateUser },
                { new: true }
              ).then(async (register_user, err) => {
                if (err) {
                  reject(err);
                } else {
                  var rmUser = await removeFromDevGroup(register_user.id);
                  console.log("remove-from-developer-Group", rmUser);
                  //payload
                  const payload = {
                    id: register_user.id,
                    fullName: register_user.fullName,
                    emailId: register_user.emailId,
                    deviceId: register_user.deviceId,
                    isAdmin: register_user.isAdmin,
                    phoneNumber: register_user.phoneNumber,
                    signUp_with: register_user.signUp_with,
                    cityLocation: register_user.cityLocation
                  };
                  //Generate Token
                  Service.generateToken(
                    payload,
                    keys.secretOrKey,
                    register_user
                  ).then((token, err) => {
                    if (err) reject(err);
                    resolve(token);
                  });
                }
              });
            }
          }
        }
      );
    } else {
      var numberMatch = false;
      var emailMatch = false;
      var numberVerify = false;
      var emailVerify = false;

      //Create PhoneNumber User
      User.findOne({ phoneNumber: params.phoneNumber }).then(
        (userPhone, err) => {
          // console.log("----USER----", user);
          if (err) {
            reject(err);
          } else {
            //check phone-number is already in database
            if (userPhone) {
              //check phone-number is verify
              numberMatch = true;
              if (userPhone.verify == true) {
                numberVerify = true;
              } else {
                numberVerify = false;
              }
            } else {
              numberMatch = false;
            }

            User.findOne({ emailId: params.emailId }).then((userEmail, err) => {
              if (err) {
                reject(err);
              } else {
                //check Email is already in database
                if (userEmail) {
                  emailMatch = true;
                  if (userEmail.verify == true) {
                    emailVerify = true;
                  } else {
                    emailVerify = false;
                  }
                } else {
                  emailMatch = false;
                }
                //if user phoneNumber and E-mail is match && verify true then exicute below code
                if (
                  (numberMatch &&
                    emailMatch &&
                    (numberVerify == true && emailVerify == true)) ||
                  (numberMatch == false &&
                    emailMatch &&
                    (numberVerify == false && emailVerify == true)) ||
                  (numberMatch &&
                    emailMatch == false &&
                    (numberVerify == true && emailVerify == false))
                ) {
                  resolve({
                    status: "Fail",
                    message: "User is already registered.",
                    data: {
                      verify: true
                    }
                  });
                }
                //if user phoneNumber or E-mail is match or not matched && verify false then exicute below code
                else if (
                  (numberMatch &&
                    emailMatch &&
                    (numberVerify == false && emailVerify == false)) ||
                  (numberMatch == false &&
                    emailMatch &&
                    (numberVerify == false && emailVerify == false)) ||
                  (numberMatch &&
                    emailMatch == false &&
                    (numberVerify == false && emailVerify == false))
                ) {
                  if (numberMatch) {
                    //console.log("numberMatch", numberMatch);
                    User.findOneAndUpdate(
                      { _id: userPhone.id },
                      {
                        $set: {
                          phoneNumber: params.phoneNumber,
                          emailId: params.emailId
                        }
                      },
                      { new: true }
                    )
                      .then(update => {
                        Service.confirmationMail(update, keys).then(
                          (confirm, err) => {
                            if (err) reject(err);
                            resolve({
                              status: "Success",
                              message: "Confirmation E-mail sent",
                              data: confirm.data
                            });
                          }
                        );
                        //console.log("Update", update);
                        //Phone-User Payload
                        // const payload = {
                        //   id: update.id,
                        //   emailId: update.emailId,
                        //   deviceId: update.deviceId,
                        //   phoneNumber: update.phoneNumber,
                        //   signUp_with: update.signUp_with,
                        //   cityLocation: update.cityLocation
                        // };
                        // //Generate Token
                        // Service.generateToken(
                        //   payload,
                        //   keys.secretOrKey,
                        //   update
                        // ).then((token, err) => {
                        //   if (err) reject(err);
                        //   resolve(token);
                        // });
                      })
                      .catch(err => reject(err));
                  } else {
                    //console.log("userEmail", numberMatch);
                    User.findOneAndUpdate(
                      { _id: userEmail.id },
                      {
                        $set: {
                          phoneNumber: params.phoneNumber,
                          emailId: params.emailId
                        }
                      },
                      { new: true }
                    )
                      .then(update => {
                        Service.confirmationMail(update, keys).then(
                          (confirm, err) => {
                            if (err) reject(err);
                            resolve({
                              status: "Success",
                              message: "Confirmation E-mail sent",
                              data: confirm.data
                            });
                          }
                        );
                        //console.log("Update", update);
                        //Phone-User Payload
                        // const payload = {
                        //   id: update.id,
                        //   emailId: update.emailId,
                        //   deviceId: update.deviceId,
                        //   phoneNumber: update.phoneNumber,
                        //   signUp_with: update.signUp_with,
                        //   cityLocation: update.cityLocation
                        // };
                        // //Generate Token
                        // Service.generateToken(
                        //   payload,
                        //   keys.secretOrKey,
                        //   update
                        // ).then((token, err) => {
                        //   if (err) reject(err);
                        //   resolve(token);
                        // });
                      })
                      .catch(err => reject(err));
                  }
                  //  if user phoneNumber and E-mail is not matched && verify false then exicute below code
                } else if (
                  numberMatch == false &&
                  numberVerify == false &&
                  emailMatch == false &&
                  emailVerify == false
                ) {
                  newUser.emailId = Data.emailId;
                  newUser.phoneNumber = Data.phoneNumber;
                  newUser.password = Data.password;
                  newUser.socialId = null;
                  newUser.verify = false;
                  newUser.image =
                    "http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/GroupImage/defaultprofile.png";
                  //password hashing
                  bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                      if (err) reject(err);
                      newUser.password = hash;
                      //create NewUser
                      new User(newUser).save((err, register_user) => {
                        //payload
                        Service.confirmationMail(register_user, keys).then(
                          (confirm, err) => {
                            if (err) reject(err);
                            resolve({
                              status: "Success",
                              message: "Confirmation E-mail sent",
                              data: confirm.data
                            });
                          }
                        );
                        // const payload = {
                        //   id: register_user.id,
                        //   emailId: register_user.emailId,
                        //   deviceId: register_user.deviceId,
                        //   phoneNumber: register_user.phoneNumber,
                        //   signUp_with: register_user.signUp_with,
                        //   cityLocation: register_user.cityLocation
                        // };
                        // if (err) {
                        //   reject(err);
                        // } else {
                        //   //Generate Token
                        //   Service.generateToken(
                        //     payload,
                        //     keys.secretOrKey,
                        //     register_user
                        //   ).then((token, err) => {
                        //     if (err) reject(err);
                        //     resolve(token);
                        //   });
                        // }
                      });
                    });
                  });
                }
              }
            });
          }
        }
      );
    }
  });
};

//----------------------------------------------------------
//Upload Image in User
exports.updateImageInput = (params, req) => {
  return new Promise((resolve, reject) => {
    var uploadImage = params.image;
    // console.log("Uplod---", uploadImage);

    var FieldToSet = { image: uploadImage };
    User.findByIdAndUpdate({ _id: params.userId }, FieldToSet, {
      new: true
    }).then((user, err) => {
      // console.log("user--", user);
      if (err) {
        reject(err);
      } else {
        if (user) {
          var image = user.image;
          resolve({
            status: "Success",
            message: "Upload Your Photo Successfully ",
            image: image
          });
        } else {
          resolve({
            status: "Fail",
            message: "User Not Register"
          });
        }
      }
    });
  });
};

//----------------------------------------------------------
//Login User Service
exports.loginUser = (params, req) => {
  return new Promise((resolve, reject) => {
    var currentDate = moment(new Date());
    //console.log("BODY--", req.body);
    // if (params.phoneNumber) {
    User.findOne({ phoneNumber: params.phoneNumber, verify: true }).then(
      async (user, err) => {
        // console.log("user----", user);
        if (err) {
          reject(err);
        } else {
          if (user) {
            //remove user from developer Group
            var rmUser = await removeFromDevGroup(user.id);
            console.log("rm-User--", rmUser);
            const updateLocation = {
              latitude: req.body.latitude,
              longitude: req.body.longitude,
              updateTime: new Date()
            };
            const updateCityLocation = {
              cityName: req.body.cityName,
              city_latitude: req.body.city_latitude,
              city_longitude: req.body.city_longitude
            };
            User.findByIdAndUpdate(
              { _id: user.id },
              {
                $set: {
                  online: true,
                  status: "green",
                  lastseen: new Date(),
                  deviceId: req.body.deviceId,
                  location: updateLocation,
                  cityLocation: updateCityLocation
                }
              },
              { new: true }
            ).then(updateUser => {
              //Payload
              const payload = {
                id: updateUser.id,
                fullName: updateUser.fullName,
                emailId: updateUser.emailId,
                deviceId: updateUser.deviceId,
                isAdmin: updateUser.isAdmin,
                phoneNumber: updateUser.phoneNumber,
                signUp_with: updateUser.signUp_with,
                cityLocation: updateUser.cityLocation
              };
              //sign token
              Service.generateToken(payload, keys.secretOrKey, updateUser).then(
                (token, err) => {
                  if (err) reject(err);
                  resolve(token);
                }
              );
            });
          } else {
            resolve({
              status: "Fail",
              message: "User Not Register"
            });
          }
        }
      }
    );
  });
};

//----------------------------------------------------------
//Login With EmailId
exports.loginWithEmailInput = (params, req) => {
  return new Promise((resolve, reject) => {
    var currentDate = moment(new Date());
    // console.log("BODY--", req.body);
    // if (params.phoneNumber) {
    User.findOne({ emailId: params.emailId, verify: true }).then(
      (user, err) => {
        // console.log("user----", user);
        if (err) {
          reject(err);
        } else {
          if (user) {
            //console.log("User---", user);
            //compare password
            bcrypt.compare(req.body.password, user.password).then(isMatch => {
              // console.log("Password-Match", isMatch);
              // console.log("Body-Password", req.body.password);
              // console.log("Db-Password", user.password);
              if (!isMatch) {
                resolve({
                  status: "Fail",
                  message: "Password incorrect"
                });
              } else {
                const updateLocation = {
                  latitude: req.body.latitude,
                  longitude: req.body.longitude,
                  updateTime: new Date()
                };
                const updateCityLocation = {
                  cityName: req.body.cityName,
                  city_latitude: req.body.city_latitude,
                  city_longitude: req.body.city_longitude
                };
                User.findByIdAndUpdate(
                  { _id: user.id },
                  {
                    $set: {
                      online: true,
                      status: "green",
                      lastseen: new Date(),
                      deviceId: req.body.deviceId,
                      location: updateLocation,
                      cityLocation: updateCityLocation
                    }
                  },
                  { new: true }
                ).then(async updateUser => {
                  //Payload
                  console.log("update-User--", updateUser.id);
                  var rmUser = await removeFromDevGroup(
                    updateUser.id || user.id
                  );
                  console.log("rm-User---", rmUser);
                  const payload = {
                    id: updateUser.id,
                    fullName: updateUser.fullName,
                    emailId: updateUser.emailId,
                    deviceId: updateUser.deviceId,
                    isAdmin: updateUser.isAdmin,
                    phoneNumber: updateUser.phoneNumber,
                    signUp_with: updateUser.signUp_with,
                    cityLocation: updateUser.cityLocation
                  };
                  //sign token
                  Service.generateToken(
                    payload,
                    keys.secretOrKey,
                    updateUser
                  ).then((token, err) => {
                    if (err) reject(err);
                    resolve(token);
                  });
                });
              }
            });
          } else {
            resolve({
              status: "Fail",
              message: "User Not Register"
            });
          }
        }
      }
    );
  });
};

//----------------------------------------------------------
//Forgot Service of User
exports.forgotInput = (params, req) => {
  return new Promise((resolve, reject) => {
    //Find Email in database
    User.findOneAndUpdate(
      { emailId: params.emailId },
      { $set: { confirmPasswordTime: new Date() } },
      { new: true }
    )
      .then(user => {
        //if No Email
        if (user) {
          const encryptedId = cryptr.encrypt(user.id);
          const transporter = nodemailer.createTransport({
            service: "Gmail",
            host: "smtp.gmail.com",
            port: 465,
            secure: true,
            auth: {
              user: keys.email,
              pass: keys.pass
            },
            logger: true,
            debug: false
          });
          const mailOptions = {
            from: "donotreply@premiumconnect.com",
            to: `${user.emailId}`,
            subject: "Link to Reset Password",
            html:
              "<h3>You are receiving this because you have to request the reset of the password for your account.</h3>" +
              "<h3>Please click on the below button to complete the process:</h3>" +
              "<a href=http://@ec2-18-222-223-24.us-east-2.compute.amazonaws.com/reset/" +
              encryptedId +
              " style='background-color: #ffa433;border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;border-radius:5px' >Reset Password</a> <br>" +
              "<h3>if you did not request this, please ignore this email and password will remain unchanged.</h3>"
          };
          transporter.sendMail(mailOptions, function(err, response) {
            if (err) {
              reject(err);
              // throw err;
            }
            console.log("RR", response);
            resolve({ status: "Success", message: "Recovery E-mail sent" });
            // res.status(200).json("recovery email sent");
          });
        } else {
          return resolve({ status: "Fail", message: "E-mail not in database" });
        }
      })
      .catch(err => reject(err));
  });
};

//----------------------------------------------------------
//Get Reset Service of User
exports.resetInput = req => {
  return new Promise((resolve, reject) => {
    const decryptedId = cryptr.decrypt(req.params.id);
    User.findOne({ _id: decryptedId })
      .then((user, err) => {
        if (err) {
          reject(err);
        } else {
          console.log("USER---", user);
          var dbDate = user.confirmPasswordTime;
          var duration = moment.duration(moment().diff(moment(dbDate)));
          var years = duration._data.years;
          var months = duration._data.months;
          var days = duration._data.days;
          var hours = duration._data.hours;
          var minutes = duration._data.minutes;
          //console.log("Duration-Minutes", duration);
          // console.log("Duration-Hours", hours);
          // console.log("Duration-Minutes", minutes);
          if ((years == 0, months == 0, days == 0, hours == 0)) {
            if (minutes < 10) {
              resolve({
                status: "Success",
                message: "password reset link a-ok"
              });
            } else {
              resolve({
                status: "Fail",
                message: "Password reset link is invalid or has expired"
              });
            }
          } else {
            resolve({
              status: "Fail",
              message: "Password reset link is invalid or has expired"
            });
          }
        }
      })
      .catch(err => reject(err));
  });
};

//----------------------------------------------------------
//ForgotPasswordViaEmail
exports.forgotPasswordInput = req => {
  //console.log("RB--", req.body);
  return new Promise((resolve, reject) => {
    const decryptedId = cryptr.decrypt(req.params.id);
    User.findOne({ _id: decryptedId }).then(user => {
      if (user) {
        //console.log("user exists in db");
        if (req.body.newPassword === req.body.confirmPassword) {
          //console.log("Match---");
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt
              .hash(req.body.confirmPassword, salt)
              .then(hashpassword => {
                User.findByIdAndUpdate(
                  { _id: user.id },
                  { password: hashpassword },
                  { new: true }
                ).then(
                  resolve({
                    status: "Success",
                    message: "Your Password is Successfull Updated"
                  })
                );
              })
              .catch(err => reject(err));
          });
        } else {
          resolve({
            status: "Fail",
            message: "Your Password Does Not Matched"
          });
        }
      } else {
        resolve({
          status: "Fail",
          message: "no user exist in db to update"
        });
      }
    });
  });
};

////////////////////Group-Services//////////////////////////

//----------------------------------------------------------
//CreateGroup Service
exports.createGroupInput = (params, req) => {
  var currentDate = moment(new Date());
  var futureMonth = moment(currentDate)
    .add(1, "M")
    .subtract(1, "day");
  //console.log("BODY---", req.body);
  console.log("Req-User", req.user.id);
  console.log("CurrentDate---", currentDate);
  console.log("futureMonth---", futureMonth);
  return new Promise(function(resolve, reject) {
    const data = req.user.cityLocation;
    const GroupFields = {
      groupName: params.groupName,
      groupType: params.groupType,
      creater: "User",
      createrId: params.loginUser,
      createdByAdmin: false,
      groupImage: params.groupImage,
      cityName: data.cityName,
      city_latitude: data.city_latitude,
      city_longitude: data.city_longitude,
      subscription: true,
      payDate: currentDate,
      payExpireDate: futureMonth
    };
    // //For Private Group
    // if (req.body.groupType == "PRIVATE") {
    //   GroupFields.subscription = true;
    //   GroupFields.payDate = currentDate;
    //   GroupFields.payExpireDate = futureMonth;
    // } else {
    //   //in public group subscription false
    //   GroupFields.subscription = false;
    // }
    GroupFields.event = {
      title: null,
      eventDate: null,
      eventTime: null,
      message: null,
      eventLocation: null
    };
    //member fields
    GroupFields.membersList = [];
    let member = {
      memberId: req.user.id,
      isSuperAdmin: true,
      isAdmin: true
    };
    GroupFields.membersList.push(member);
    req.body.memberIds = JSON.parse(req.body.memberIds);
    req.body.memberIds.forEach(member => {
      GroupFields.membersList.push({
        memberId: member,
        isSuperAdmin: false,
        isAdmin: false
      });
    });

    //Only For Admin User
    if (req.user.isAdmin) {
      GroupFields.createdByAdmin = true;
      GroupFields.payDate = null;
      GroupFields.payExpireDate = null;
      console.log("AdminUser--", req.user.isAdmin);
      new Groups(GroupFields).save().then(group => {
        resolve({
          status: "Success",
          data: group
        });
      });
    } else {
      //console.log("data----", GroupFields);
      //create Group
      new Groups(GroupFields)
        .save()
        .then(group => {
          if (
            req.body.groupType == "PRIVATE" ||
            req.body.groupType == "PUBLIC"
          ) {
            const PaymentFields = {
              payFor: "Group",
              receipt: req.body.receipt,
              userId: group.createrId,
              groupId: group._id,
              groupName: group.groupName,
              Amount: req.body.Amount,
              payDate: currentDate,
              payExpireDate: futureMonth
            };
            //create Payment
            new Payment(PaymentFields)
              .save()
              .then(payment => {
                resolve({
                  status: "Success",
                  message: "Group & Payment successfully Created",
                  data: group,
                  payment
                });
              })
              .catch(err => reject(err));
          } else {
            resolve({
              status: "Success",
              message: "Group successfully Created",
              data: group
            });
          }
        })
        .catch(err => reject(err));
    }
  });
};

//----------------------------------------------------------
//updateGroup Service
exports.updateGroupInput = (params, req) => {
  //console.log("REQ---", req.body);
  return new Promise(function(resolve, reject) {
    var query = [
      {
        $match: { _id: params.groupId }
      },
      {
        $project: {
          membersList: {
            $filter: {
              input: "$membersList",
              as: "item",
              cond: {
                $eq: ["$$item.memberId", params.loginUser]
              }
            }
          }
        }
      }
    ];
    const groupFields = {};
    if (req.body.groupName) groupFields.groupName = req.body.groupName;
    //if (req.body.groupType) groupFields.groupType = req.body.groupType;
    //console.log("Group-Name", req.body.groupName);

    Groups.aggregate(query, (error, isMatch) => {
      // console.log("isMatch", isMatch);
      if (error) {
        reject(error);
      } else {
        if (isMatch[0].membersList.length != 0) {
          Groups.findByIdAndUpdate(
            { _id: params.groupId },
            { $set: groupFields },
            { new: true }
          )
            .then((group, err) => {
              //console.log("Group---", group);
              if (err) {
                reject(err);
              } else {
                if (groupFields != null) {
                  resolve({
                    status: "Success",
                    message: "Update Successfully",
                    data: group
                  });
                } else {
                  resolve({
                    status: "Fail",
                    message: "You have to not Update any field"
                  });
                }
              }
            })
            .catch(err => reject(err));
        } else {
          resolve("You are Not a Group Member ");
        }
      }
    });
  });
};

//----------------------------------------------------------
//Get All Group of User Service
exports.getAllGroupInput = (params, req) => {
  return new Promise(async function(resolve, reject) {
    var query = [
      {
        $match: {
          groupType: params.groupType,
          // "membersList.memberId": params.loginUser,
          membersList: {
            $elemMatch: {
              memberId: params.loginUser,
              blockUser: false
            }
          }
        }
      },
      {
        $lookup: {
          from: "users",
          as: "userData",
          localField: "membersList.memberId",
          foreignField: "_id"
        }
      },
      {
        $project: {
          "userData.online": 1,
          "userData.status": 1,
          groupName: 1,
          groupType: 1,
          creater: 1,
          createdByAdmin: 1,
          groupImage: 1,
          payment: 1,
          subscription: 1,
          membersList: 1,
          cityName: 1,
          city_latitude: 1,
          city_longitude: 1,
          payExpireDate: 1
        }
      }
    ];
    var query1 = [
      {
        $match: { createrId: params.loginUser }
      },
      {
        $lookup: {
          from: "users",
          as: "userData",
          localField: "membersList.memberId",
          foreignField: "_id"
        }
      },
      {
        $project: {
          "userData.online": 1,
          "userData.status": 1,
          groupName: 1,
          groupType: 1,
          creater: 1,
          createdByAdmin: 1,
          groupImage: 1,
          payment: 1,
          subscription: 1,
          membersList: 1,
          cityName: 1,
          city_latitude: 1,
          city_longitude: 1,
          payExpireDate: 1
        }
      }
    ];

    if (req.body.groupType == "PUBLIC") {
      //set notification badge Zero(0)
      await User.findByIdAndUpdate(
        { _id: params.loginUser },
        { $set: { notificationBadge: 0 } },
        { new: true }
      ).then(user => {
        console.log("Noti-Badge--", user.notificationBadge);
      });
      //////////////////////////////////////
      var checkPubGrpSub = await updatePublicGrpSub(params);
      //////////////////////////////////////

      console.log("------After-update-subscription-----");
      //for Public group
      Groups.aggregate(query, (err, isMatchs) => {
        // console.log("===match===", isMatchs[0]._id);
        // console.log("===Length===", isMatchs.length);
        if (err) reject(err);
        if (isMatchs) {
          resolve({
            status: "Success",
            data: isMatchs
          });
        } else {
          resolve({
            status: "Fail",
            message: `There are no ${params.groupType} group`
          });
        }
      });
    } else if (req.body.groupType == "PRIVATE") {
      const currentDate = new Date();
      ///////////////////////
      var checkSub = await updateGrpSubscription(params);
      console.log("check-Subscription--", checkSub);
      /////////////////////////
      Groups.aggregate(query, (err, isMatchs) => {
        console.log("--------In Main code---------");
        if (err) {
          reject(err);
        } else {
          // console.log("isMatchs", isMatchs);
          if (isMatchs.length != 0) {
            //Only For Admin User
            if (req.user.isAdmin) {
              console.log("Admin-User--", req.user.isAdmin);
              resolve({
                status: "Success",
                data: isMatchs
              });
            } else {
              resolve({
                status: "Success",
                data: isMatchs
              });
            }
          } else {
            resolve({
              status: "Fail",
              message: `There are no ${params.groupType} group`,
              data: isMatchs
            });
          }
        }
      });
    } else {
      if (req.body.groupType == "MYGROUPS") {
        Groups.aggregate(query1, (err, group) => {
          // console.log("Group---", group);
          if (err) reject(err);
          if (group.length != 0) {
            resolve({
              status: "Success",
              data: group
            });
          } else {
            resolve({
              status: "Fail",
              message: "You have to not create any Group"
            });
          }
        });
      }
    }
  });
};

//this function related to above getallgroup api
function updateGrpSubscription(params) {
  console.log("-----------------In-Update-Subscription---------------");
  const currentDate = new Date();
  var query = [
    {
      $match: {
        groupType: params.groupType,
        // "membersList.memberId": params.loginUser,
        membersList: {
          $elemMatch: {
            memberId: params.loginUser,
            blockUser: false
          }
        }
      }
    }
  ];
  return new Promise((resolve, reject) => {
    Groups.aggregate(query, (err, getGroup) => {
      (async function checkGroupSubscription() {
        Counter = 0;
        for (i = 0; i < getGroup.length; i++) {
          console.log("----------In-Looop-::" + i + "-------------");
          if (getGroup[i].createdByAdmin) {
            Counter++;
            //no update subscription key
          } else {
            Counter++;
            //check for other
            const expireDate = getGroup[i].payExpireDate;
            if (expireDate <= currentDate) {
              await Groups.findOneAndUpdate(
                { _id: getGroup[i]._id },
                { subscription: false },
                { new: true }
              ).then(update1 => {
                // console.log("Update--", update1);
                // AllGroups.push(update1);
              });
            } else {
              // AllGroups.push(getGroup[i]);
            }
          }
        }
        if (getGroup.length == Counter) {
          console.log("-------Success---------");
          resolve("Success");
        } else {
          console.log("-------Error---------");
          reject("Fail");
        }
      })();
    });
  });
}

//this function is update public group Subscription
function updatePublicGrpSub(params) {
  console.log("-----------------In-Update-Subscription---------------");
  const currentDate = new Date();
  var query = [
    {
      $match: {
        groupType: params.groupType,
        creater: "User",
        // "membersList.memberId": params.loginUser,
        membersList: {
          $elemMatch: {
            memberId: params.loginUser,
            blockUser: false
          }
        }
      }
    }
  ];
  return new Promise((resolve, reject) => {
    Groups.aggregate(query, (err, getGroup) => {
      (async function checkGroupSubscription() {
        Counter = 0;
        for (i = 0; i < getGroup.length; i++) {
          console.log("----------In-Looop-::" + i + "-------------");
          if (getGroup[i].createdByAdmin) {
            Counter++;
            //no update subscription key, this user is admin
          } else {
            Counter++;
            //check for other
            const expireDate = getGroup[i].payExpireDate;
            if (expireDate <= currentDate) {
              await Groups.findOneAndUpdate(
                { _id: getGroup[i]._id },
                { subscription: false },
                { new: true }
              ).then(update1 => {
                // console.log("Update--", update1);
                // AllGroups.push(update1);
              });
            } else {
              // AllGroups.push(getGroup[i]);
            }
          }
        }
        if (getGroup.length == Counter) {
          console.log("-------Success---------");
          resolve("Success");
        } else {
          console.log("-------Error---------");
          reject("Fail");
        }
      })();
    });
  });
}

//----------------------------------------------------------
//Get Group
exports.GetGroup = (params, req) => {
  var currentDate = moment(new Date());
  return new Promise((resolve, reject) => {
    var query = [
      {
        $match: { _id: params.groupId }
      },
      {
        $project: {
          membersList: {
            $filter: {
              input: "$membersList",
              as: "item",
              cond: { $eq: ["$$item.memberId", params.loginUser] }
            }
          }
        }
      }
    ];
    var query1 = [
      {
        $match: { _id: params.groupId }
      },
      {
        $unwind: "$membersList"
      },
      {
        $lookup: {
          from: "users",
          as: "userData",
          localField: "membersList.memberId",
          foreignField: "_id"
        }
      },
      {
        $unwind: "$userData"
      },
      {
        $project: {
          event: 1,
          cityName: 1,
          userData: 1,
          // "userData._id": 1,
          // "userData.fullName": 1,
          // "userData.image": 1,
          // "userData.lastseen": 1,
          // "userData.location": 1,
          // "userData.online": 1,
          // "userData.status": 1,
          "membersList.blockUser": 1,
          "membersList.isSuperAdmin": 1,
          "membersList.isAdmin": 1
        }
      }
    ];

    Groups.aggregate(query, (err, isMatch) => {
      // console.log("Is-Match", isMatch);
      if (err) {
        reject(err);
      } else {
        if (isMatch[0].length != 0) {
          Groups.aggregate(query1, async (err, userData) => {
            // console.log("userData---", userData);
            if (err) {
              reject(err);
            } else {
              if (err) {
                reject(err);
              } else {
                if (userData.length != 0) {
                  var AllData = [];
                  var event = userData[0].event;
                  var groupId = userData[0]._id;
                  var cityName = userData[0].cityName;
                  //console.log("Event---", event);
                  userData.forEach(user => {
                    AllData.push({
                      membersList: user.membersList,
                      userData: user.userData
                    });
                  });
                  //console.log("AllData---", AllData);
                  var eventTime = event.eventTime || null;
                  //console.log("Event-Time", eventTime);
                  if (eventTime) {
                    //console.log("Event-Time", eventTime);
                    if (currentDate.isAfter(eventTime)) {
                      const eventData = {
                        eventDate: null,
                        eventTime: null,
                        message: null,
                        eventLocation: null
                      };
                      await Groups.findByIdAndUpdate(
                        { _id: groupId },
                        { $set: { event: eventData } },
                        { new: true }
                      ).then(group => {
                        var newEvent = group.event;
                        //console.log("New-Event", newEvent);
                        resolve({
                          status: "Success",
                          event: newEvent,
                          groupId: groupId,
                          cityName: cityName,
                          data: AllData
                        });
                      });
                    } else {
                      resolve({
                        status: "Success",
                        event: event,
                        groupId: groupId,
                        cityName: cityName,
                        data: AllData
                      });
                    }
                  }
                  resolve({
                    status: "Success",
                    event: event,
                    groupId: groupId,
                    cityName: cityName,
                    data: AllData
                  });
                } else {
                  resolve({
                    status: "Fail",
                    message: "Group Details NotFound"
                  });
                }
              }
            }
          });
        } else {
          resolve({
            status: "Fail",
            message: "You are not a group member"
          });
        }
      }
    });
  });
};

//----------------------------------------------------------
//Delete Group Service
exports.deletGroupInput = (params, req) => {
  return new Promise(function(resolve, reject) {
    var query = [
      {
        $match: { _id: params.groupId }
      },
      {
        $project: {
          membersList: {
            $filter: {
              input: "$membersList",
              as: "item",
              cond: {
                $and: [
                  { $eq: ["$$item.memberId", params.loginUser] },
                  { $eq: ["$$item.isSuperAdmin", true] }
                ]
              }
            }
          }
        }
      }
    ];

    Groups.aggregate(query, (err, isMatch) => {
      if (err) reject(err);
      //console.log("Is-Match---", isMatch);
      if (isMatch[0].membersList.length != 0) {
        Groups.findByIdAndDelete({ _id: params.groupId }).then(group => {
          if (group) {
            const removegroup = _.remove(group);

            resolve({ message: "Group deleted", data: removegroup });
          } else {
            resolve("group not found");
          }
        });
      } else {
        resolve("Group not found OR Member is not a SuperAdmin or Admin");
      }
    });
  });
};

//---------------------------------------------------------
//Left Group Service
exports.leftGroupInput = (params, req) => {
  return new Promise((resolve, reject) => {
    var query = [
      {
        $match: {
          _id: params.groupId
        }
      },
      {
        $unwind: "$membersList"
      },
      {
        $match: {
          "membersList.memberId": params.loginUser
        }
      }
    ];
    var query1 = [
      {
        $match: {
          _id: params.groupId,
          "membersList.memberId": params.loginUser
        }
      }
    ];

    Groups.aggregate(query, (err, user) => {
      console.log("User--", user);
      console.log("SuperAdmin--", user[0].membersList.isSuperAdmin);
      const SuperAdmin = user[0].membersList.isSuperAdmin;
      if (err) reject(err);
      else {
        //if leftGroup User is SuperAdmin
        if (SuperAdmin == true) {
          Groups.aggregate(query1, async (err, isMatch) => {
            if (err) reject(err);
            if (isMatch.length != 0) {
              //superAdmin remove from the group
              removeMember(isMatch, params.groupId).then(newUser => {
                if (newUser) {
                  //assign isSuperAdmin and isAdmin to other user
                  Groups.findOneAndUpdate(
                    { _id: params.groupId },
                    {
                      $set: {
                        "membersList.0.isSuperAdmin": true,
                        "membersList.0.isAdmin": true
                      }
                    },
                    { new: true }
                  ).then(update => {
                    console.log("Check--", update.membersList.length);
                    if (update.membersList.length === 0) {
                      Groups.findByIdAndDelete({ _id: params.groupId }).then(
                        msg =>
                          resolve({
                            message:
                              "All member are Left thats why this group is deleted "
                          })
                      );
                    } else {
                      resolve(update);
                      console.log("Update-User--", update);
                    }
                  });
                } else {
                  resolve({
                    status: "Fail",
                    message: "User Not Removed"
                  });
                }
              });
              // });
            } else {
              resolve({
                status: "Fail",
                message: "You are not a GroupMember"
              });
            }
          });
        } else {
          Groups.aggregate(query1, (err, isMatch) => {
            if (err) reject(err);
            if (isMatch.length != 0) {
              removeMember(isMatch, params.groupId).then(newUser => {
                resolve({
                  status: "Success",
                  newUser
                });
              });
            } else {
              resolve({
                status: "Fail",
                message: "You are not a GroupMember"
              });
            }
          });
        }
      }
    });

    // Groups.aggregate(query, (err, isMatch) => {
    //   if (err) reject(err);
    //   if (isMatch.length != 0) {
    //     _.remove(
    //       isMatch[0].membersList,
    //       group => group.memberId == req.user.id
    //     );
    //     var updatedFields = {
    //       membersList: isMatch[0].membersList
    //     };
    //     Groups.findByIdAndUpdate(
    //       params.groupId,
    //       updatedFields,
    //       { new: true },
    //       (err, user) => {
    //         // console.log("usssssssss", user);
    //         if (err) reject(err);
    //         else {
    //           resolve(user);
    //         }
    //         // console.log("array==========", user.membersList);
    //       }
    //     );
    //     // });
    //   } else {
    //     resolve({
    //       status: "Fail",
    //       message: "You are not a GroupMember"
    //     });
    //   }
    // });
  });
  function removeMember(Data, groupId) {
    return new Promise(async newUser => {
      _.remove(Data[0].membersList, group => group.memberId == req.user.id);
      var updatedFields = {
        membersList: Data[0].membersList
      };
      await Groups.findByIdAndUpdate(params.groupId, updatedFields, {
        new: true
      }).then(user => {
        newUser(user);
      });
    });
  }
};

//----------------------------------------------------------
//Add User Service
exports.addUserInGroup = (params, req) => {
  return new Promise(function(resolve, reject) {
    var query = [
      {
        $match: { _id: params.groupId }
      },
      {
        $project: {
          membersList: {
            $filter: {
              input: "$membersList",
              as: "item",
              cond: {
                $and: [
                  { $eq: ["$$item.memberId", params.loginUser] },
                  {
                    $or: [
                      { $eq: ["$$item.isSuperAdmin", true] },
                      { $eq: ["$$item.isAdmin", true] }
                    ]
                  }
                ]
              }
            }
          }
        }
      }
    ];

    var query1 = [
      {
        $match: { _id: params.groupId }
      },
      {
        $project: {
          membersList: {
            $filter: {
              input: "$membersList",
              as: "item",
              cond: { $eq: ["$$item.memberId", params.memberId] }
            }
          }
        }
      }
    ];

    Groups.aggregate(query, (error, isMatch) => {
      if (error) reject(error);
      //console.log("loginUser", isMatch);
      if (isMatch[0].membersList.length != 0) {
        //
        Groups.aggregate(query1, (err, matchData) => {
          if (err) reject(err);
          if (matchData[0].membersList.length != 0) {
            resolve("Member Already In Group");
          } else {
            var storedata = {
              memberId: req.body.memberId,
              subscription: req.body.subscription,
              isSuperAdmin: false,
              isAdmin: false
            };
            var sync = true;
            Groups.updateOne(
              { _id: params.groupId },
              { $push: { membersList: storedata } },
              { new: true },
              (err, group) => {
                if (err) reject(err);
                sync = false;
                resolve(group);
              }
            );
            while (sync) {
              require("deasync").sleep(30);
            }
          }
        });
      } else {
        resolve("Group not found OR Member is not a SuperAdmin or Admin");
      }
    });
  });
};

//----------------------------------------------------------
//remove User Service
exports.removeUserInput = (params, req) => {
  return new Promise(function(resolve, reject) {
    var query = [
      {
        $match: { _id: params.groupId }
      },
      {
        $unwind: "$membersList"
      },
      {
        $match: {
          $and: [
            { "membersList.memberId": params.loginUser },
            {
              $or: [
                { "membersList.isSuperAdmin": true },
                { "membersList.isAdmin": true }
              ]
            }
          ]
        }
      }
    ];
    var query1 = [
      {
        $match: { _id: params.groupId }
      },
      {
        $project: { membersList: 1 }
      }
    ];
    Groups.aggregate(query, (err, users) => {
      if (err) reject(err);
      //console.log("users------", users);
      if (users.length != 0) {
        Groups.aggregate(query1, (error, removeUser) => {
          if (error) reject(error);
          //console.log("RRR", removeUser);
          _.remove(
            removeUser[0].membersList,
            group => group.memberId == req.body.memberId
          );
          var groupFields = {
            membersList: removeUser[0].membersList
          };
          Groups.findByIdAndUpdate(
            params.groupId,
            groupFields,
            { new: true },
            (err, user) => {
              //console.log("usssssssss", user);
              if (err) reject(err);
              else {
                resolve(user);
              }
              //console.log("array==========", user.membersList);
            }
          );
        });
      } else {
        resolve("Not Eligible");
      }
    });
  });
};

//----------------------------------------------------------
//Update GroupOfUser Service
exports.updateGroupMemberInput = (params, req) => {
  return new Promise(function(resolve, reject) {
    var query = [
      {
        $match: { _id: params.groupId }
      },
      {
        $project: {
          membersList: {
            $filter: {
              input: "$membersList",
              as: "item",
              cond: {
                $and: [
                  { $eq: ["$$item.memberId", params.loginUser] },
                  {
                    $or: [
                      { $eq: ["$$item.isSuperAdmin", true] },
                      { $eq: ["$$item.isAdmin", true] }
                    ]
                  }
                ]
              }
            }
          }
        }
      }
    ];
    // var query1 = []
    Groups.aggregate(query, (err, isMatch) => {
      if (err) reject(err);
      if (isMatch[0].membersList != 0) {
        Groups.updateOne(
          {
            _id: params.groupId,
            "membersList.memberId": params.memberId
          },
          {
            $set: {
              "membersList.$.isAdmin": req.body.isAdmin
            }
          }
        ).then(group => {
          //console.log("==group======", group);
          resolve({
            status: "Success",
            message: "Member Update Successfully",
            group: group
          });
        });
      } else {
        resolve({
          status: "Fail",
          message: "You are not a admin or superAdmin"
        });
      }
    });
  });
};

//----------------------------------------------------------
//Get MyTransctions Service
exports.getTransactions = (params, req) => {
  return new Promise((resolve, reject) => {
    var query = [
      {
        $match: { userId: params.loginUser }
      }
    ];

    Payment.aggregate(query, (err, group) => {
      console.log("paymets----", group);
      if (err) {
        reject(err);
      } else {
        if (group.length != 0) {
          resolve({
            status: "Success",
            data: group
          });
        } else {
          resolve({
            status: "Fail",
            message: "Groups Not Found"
          });
        }
      }
    });
  });
};

//-----------------------------------------------------------
//LogOut User servise
exports.logout = (params, req) => {
  var query = [
    {
      $match: {
        creater: "Developer",
        "membersList.memberId": params.loginUser
      }
    }
  ];
  return new Promise((resolve, reject) => {
    User.findByIdAndUpdate(
      { _id: params.loginUser },
      {
        $set: {
          online: false,
          deviceId: null,
          status: "red",
          lastseen: new Date()
        }
      },
      { new: true }
    ).then((user, err) => {
      // console.log("User---", user);
      if (err) {
        reject(err);
      } else {
        if (user) {
          Groups.aggregate(query, (err, isMatch) => {
            // console.log("Is-Match", isMatch);
            if (err) {
              reject(err);
            } else {
              // console.log("User----", params.loginUser);
              if (isMatch.length != 0) {
                Groups.aggregate(
                  [
                    {
                      $match: { _id: isMatch[0]._id }
                    },
                    {
                      $project: { membersList: 1 }
                    }
                  ],
                  (error, removeUser) => {
                    if (error) reject(error);
                    // console.log("RRR", removeUser);
                    _.remove(
                      removeUser[0].membersList,
                      group => group.memberId == req.user.id
                    );
                    var groupFields = {
                      membersList: removeUser[0].membersList
                    };
                    Groups.findByIdAndUpdate(
                      { _id: isMatch[0]._id },
                      groupFields,
                      { new: true },
                      (err, user) => {
                        // console.log("usssssssss", user);
                        if (err)
                          reject({
                            status: "Fail",
                            err: err
                          });
                        else {
                          resolve({
                            status: "Success",
                            data: user
                          });
                        }
                        // console.log("array==========", user.membersList);
                      }
                    );
                  }
                );
              } else {
                resolve({
                  status: "Success",
                  message: "Group Not Found"
                });
              }
            }
          });
        } else {
          resolve({
            status: "Fail",
            message: "User Online Status Not Update"
          });
        }
      }
    });
  });
};

//------------------------------------------------------------
//this is generateToken function used in registration & login api
exports.generateToken = (payload, key, Data) => {
  console.log("payload---", payload);
  console.log("key---", key);
  console.log("Response-Data---", Data);
  return new Promise((tokenData, error) => {
    jwt.sign(
      payload,
      key,
      // { expiresIn: 3600 },
      (err, token) => {
        if (err) {
          // if token not generate
          error({
            status: "Fail",
            message: "Token Not Generate",
            err: err
          });
        } else {
          //token generate
          tokenData({
            status: "Success",
            message: "User Register Successfully",
            token: token,
            user: Data
          });
        }
      }
    );
  });
};

//--------------------------------------------------------------
//when user register send conformation mail on his emailId.
exports.confirmationMail = (user, keys) => {
  const encryptedId = cryptr.encrypt(user.id);
  return new Promise((resolve, reject) => {
    console.log("User---", user);
    const transporter = nodemailer.createTransport({
      service: "Gmail",
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      auth: {
        user: keys.email,
        pass: keys.pass
      },
      logger: true,
      debug: false
    });
    const mailOptions = {
      from: "donotreply@premiumconnect.com",
      to: `${user.emailId}`,
      subject: "Account Verification",
      html:
        "<h2>Confirm Your Email</h2><br>" +
        "Dear " +
        user.fullName +
        "<br>" +
        "We Received a request to set your Premium Solutions E-mail to " +
        user.emailId +
        " and PhoneNumber is " +
        user.countryCode +
        " " +
        user.phoneNumber +
        ".<br>" +
        "If this is correct,please confirm by clicking the button below.<br><br>" +
        "<a href=http://@ec2-18-222-223-24.us-east-2.compute.amazonaws.com/updateVerify/" +
        encryptedId +
        " style='background-color: #ffa433;border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;border-radius:5px' >Verify</a>"
    };
    transporter.sendMail(mailOptions, function(err, response) {
      if (err) {
        reject(err);
        // throw err;
      }
      console.log("RR", response);
      resolve({
        status: "Success",
        message: "Confirmation E-mail sent",
        data: user
      });
      // res.status(200).json("recovery email sent");
    });
  });
};

//test-------getall group api
//----------------------------------------------------------
//Get All Group of User Service
exports.getAllGroupInput1 = (params, req) => {
  return new Promise(async function(resolve, reject) {
    var query = [
      {
        $match: {
          groupType: params.groupType,
          // "membersList.memberId": params.loginUser,
          membersList: {
            $elemMatch: {
              memberId: params.loginUser,
              blockUser: false
            }
          }
        }
      },
      {
        $lookup: {
          from: "users",
          as: "userData",
          localField: "membersList.memberId",
          foreignField: "_id"
        }
      },
      {
        $project: {
          "userData.online": 1,
          "userData.status": 1,
          groupName: 1,
          groupType: 1,
          creater: 1,
          createdByAdmin: 1,
          groupImage: 1,
          payment: 1,
          subscription: 1,
          membersList: 1,
          cityName: 1,
          city_latitude: 1,
          city_longitude: 1,
          payExpireDate: 1
        }
      }
    ];
    var query1 = [
      {
        $match: { createrId: params.loginUser }
      },
      {
        $lookup: {
          from: "users",
          as: "userData",
          localField: "membersList.memberId",
          foreignField: "_id"
        }
      },
      {
        $project: {
          "userData.online": 1,
          "userData.status": 1,
          groupName: 1,
          groupType: 1,
          creater: 1,
          createdByAdmin: 1,
          groupImage: 1,
          payment: 1,
          subscription: 1,
          membersList: 1,
          cityName: 1,
          city_latitude: 1,
          city_longitude: 1,
          payExpireDate: 1
        }
      }
    ];

    if (req.body.groupType == "PUBLIC") {
      //set notification badge Zero(0)
      User.findByIdAndUpdate(
        { _id: params.loginUser },
        { $set: { notificationBadge: 0 } },
        { new: true }
      ).then(user => {
        console.log("Noti-Badge--", user.notificationBadge);
      });
      //for Public group
      Groups.aggregate(query, (err, isMatchs) => {
        // console.log("===match===", isMatchs[0]._id);
        // console.log("===Length===", isMatchs.length);
        if (err) reject(err);
        if (isMatchs) {
          resolve({
            status: "Success",
            data: isMatchs
          });
        } else {
          resolve({
            status: "Fail",
            message: `There are no ${params.groupType} group`
          });
        }
      });
    } else if (req.body.groupType == "PRIVATE") {
      const currentDate = new Date();
      ///////////////////////
      var checkSub = await updateGrpSubscription(params);
      console.log("check-Subscription--", checkSub);
      /////////////////////////
      Groups.aggregate(query, (err, isMatchs) => {
        console.log("--------In Main code---------");
        if (err) {
          reject(err);
        } else {
          // console.log("isMatchs", isMatchs);
          if (isMatchs.length != 0) {
            //Only For Admin User
            if (req.user.isAdmin) {
              console.log("Admin-User--", req.user.isAdmin);
              resolve({
                status: "Success",
                data: isMatchs
              });
            } else {
              resolve({
                status: "Success",
                data: isMatchs
              });
            }
          } else {
            resolve({
              status: "Fail",
              message: `There are no ${params.groupType} group`,
              data: isMatchs
            });
          }
        }
      });
    } else {
      if (req.body.groupType == "MYGROUPS") {
        Groups.aggregate(query1, (err, group) => {
          // console.log("Group---", group);
          if (err) reject(err);
          if (group.length != 0) {
            resolve({
              status: "Success",
              data: group
            });
          } else {
            resolve({
              status: "Fail",
              message: "You have to not create any Group"
            });
          }
        });
      }
    }
  });
};

//when user login at the time remove from Developer Group
function removeFromDevGroup(memberId) {
  return new Promise(async resolve => {
    console.log("---------Before-MemberId----", memberId);
    memberId = mongoose.Types.ObjectId(memberId);
    console.log("---------After-MemberId----", memberId);
    var check = await Groups.findOne({
      creater: "Developer",
      "membersList.memberId": memberId
    });

    var strMemberId = memberId.toString();

    console.log("Check---", check);

    if (check) {
      //remove user from developer group
      _.remove(check.membersList, group => group.memberId == strMemberId);

      var groupFields = {
        membersList: check.membersList
      };

      await Groups.findByIdAndUpdate(
        { _id: check._id },
        { $set: groupFields },
        { new: true }
      ).then(result => {
        if (!result) {
          resolve("Error");
        } else {
          resolve(result);
        }
      });
    } else {
      resolve("Error");
    }
  });
}

//genrate Random String
exports.randomStringGenerator = function() {
  let text = "";
  let possible = "0123456789";
  for (let i = 0; i < 6; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
};
