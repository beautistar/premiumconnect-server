const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const engines = require("consolidate");

const app = express();

app.engine("ejs", engines.ejs);
app.set("views", "./views");
app.set("view engine", "ejs");

//Middleware
app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(bodyParser.json());
app.use("/Images", express.static(__dirname + "/Images"));
app.use("/GroupImage", express.static(__dirname + "/defaultImage"));
// Db config
const db = require("./config/keys").mongoURI;

//Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log("MongoDB Connected"))
  .catch(err => console.log(err));

// Routes
require("./Routes")(app);
//app.use("/api", index);

//
const port = process.env.PORT || 8080;

app.listen(port, () => console.log(`Server running on port ${port}`));
